const easyProjectScp = require("easy-project-scp");
const path = require("path");
const HappyPack = require("happypack");
const os = require("os");
const happyThread = HappyPack.ThreadPool({ size: os.cpus().length });
const speedMeasurePlugin = require("speed-measure-webpack-plugin");
const smp = new speedMeasurePlugin();
module.exports = {
  publicPath: "/",
  parallel: false,
  css: {
    modules: false,
    loaderOptions: {
      //全局变量
      sass: {
        data: `@import "@/_css/config.scss";`
      }
    }
  },
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      alias: {
        "@utils": path.join(__dirname, "./src/utils"),
        "@mixins": path.join(__dirname, "./src/mixins"),
        "@components": path.join(__dirname, "./src/components")
      }
    },
    plugins: smp.wrap([
      new easyProjectScp.WebpackPlugin(),
      new HappyPack({
        id: "js",
        loaders: [
          { loader: "babel-loader", query: "?presets[]=es2015" },
          { loader: "thread-loader", option: { Worker: 2 } }
        ],
        threadPool: happyThread,
        verbose: true
      }),
      new HappyPack({ id: "styles", loaders: [{ loader: "thread-loader", option: { Worker: 2 } }], threadPool: happyThread, loaders: ["sass-loader"] })
    ]),
    externals: {
      vue: "Vue",
      vuex: "Vuex",
      "vue-router": "VueRouter",
      axios: "axios",
      qs: "Qs"
    }
  },
  // configureWebpack: (config) => {
  //   // config.devtools = process.env.NODE_ENV === 'development'
  //   if (process.env.NODE_ENV === "production") {
  //     config.plugins.push(new easyProjectScp.WebpackPlugin());
  //     config.plugins.push(
  //       new HappyPack({
  //         id: "js",
  //         loaders: [
  //           {
  //             loader: "babel-loader",
  //             query: "?presets[]=es2015",
  //           },
  //         ],
  //         threadPool: happyThread,
  //         verbose: true,
  //       })
  //     );
  //     config.plugins.push(
  //       new HappyPack({
  //         id: "styles",
  //         threadPool: happyThread,
  //         loaders: ["sass-loader"],
  //       })
  //     );

  //     // 生产环境
  //     // config.plugins.push(
  //     //   new CompressionWebpackPlugin({
  //     //     asset: "[path].gz[query]",
  //     //     algorithm: "gzip",
  //     //     test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
  //     //     threshold: 10240,
  //     //     minRatio: 0.8
  //     //   })
  //     // );
  //   } else {
  //     // 开发环境
  //   }
  //   config["externals"] = {
  //     vue: "Vue",
  //     vuex: "Vuex",
  //     "vue-router": "VueRouter",
  //     axios: "axios",
  //     qs: "Qs",
  //   };
  // },
  chainWebpack: config => {}
};
