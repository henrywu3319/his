 <p align="center">
   <img src="https://img.shields.io/badge/Release-V3.5.0-green.svg" alt="Downloads">
   <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="Build Status">
   <img src="https://img.shields.io/badge/license-Apache%202-blue.svg" alt="Build Status">
   <img src="https://img.shields.io/badge/Spring%20Cloud-2021-blue.svg" alt="Coverage Status">
   <img src="https://img.shields.io/badge/Spring%20Boot-2.7.1-blue.svg" alt="Downloads">
   <a target="_blank" href="https://bladex.vip">
   <img src="https://img.shields.io/badge/Author-Small%20Chill-ff69b4.svg" alt="Downloads">
 </a>
 </p>  

## 云门诊系统
* 后端采用SpringCloud全家桶，前端Vue
* 集成Sentinel从流量控制、熔断降级、系统负载等多个维度保护服务的稳定性。
* 注册中心、配置中心选型EK，为工程瘦身的同时加强各模块之间的联动。
* 极简封装了多租户底层，用更少的代码换来拓展性更强的SaaS多租户系统。
* 借鉴OAuth2，实现了多终端认证系统，可控制子系统的token权限互相隔离。
* 项目分包明确，规范微服务的开发模式，使包与包之间的分工清晰。

## 在线演示
* 地址：[http://his.sudawuye.com/](http://his.sudawuye.com/)
* 账号：zsf   密码：123456
* 后台配置系统演示，请加
<img src="https://gitee.com/leearmy/his/raw/master/assets/ewm.png"/>

# 开源协议
Apache Licence 2.0 （[英文原文](http://www.apache.org/licenses/LICENSE-2.0.html)）
Apache Licence是著名的非盈利开源组织Apache采用的协议。该协议和BSD类似，同样鼓励代码共享和尊重原作者的著作权，同样允许代码修改，再发布（作为开源或商业软件）。
需要满足的条件如下：
* 需要给代码的用户一份Apache Licence
* 如果你修改了代码，需要在被修改的文件中说明。
* 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明。
* 如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有Apache Licence。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。
Apache Licence也是对商业应用友好的许可。使用者也可以在需要的时候修改代码来满足需要并作为开源或商业产品发布/销售。

## 用户权益
* 允许免费用于学习、毕设、公司项目、私活等，但请保留源码作者信息。
* 对未经过授权和不遵循 Apache 2.0 协议二次开源或者商业化我们将追究到底。

# 界面

## [云门诊](http://his.sudawuye.com/) 挂号分诊
<table>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/gzt.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/xzhy.jpg"/></td>
    </tr>
</table>

## [云门诊](http://his.sudawuye.com/) 门诊流程
<table>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/mzbl.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/mzcf.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/mzzl.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/cfyp.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/sysjc.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/fzjc.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/qtfy.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/ghf.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/jcsys.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/jcfz.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/jczl.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/fy.jpg"/></td>
    </tr>
</table>

## [云门诊](http://his.sudawuye.com/) 经营管理
<table>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/yy.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/pb.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/jh.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/cfyp.jpg"/></td>
    </tr>
</table>

## [云门诊](http://his.sudawuye.com/) 诊所
<table>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/glsf.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/glhz.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/glypk.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/glbb.jpg"/></td>
    </tr>
</table>

## [云门诊](http://his.sudawuye.com/) 配置后台一览
<table>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/kssz.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/syssf.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/xygl.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/zygl.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/tcgl.jpg"/></td>
        <td><img src="https://gitee.com/leearmy/his/raw/master/assets/mbgl.jpg"/></td>
    </tr>
</table>

   


