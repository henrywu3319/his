const path = require("path");
module.exports = {
  // 暂时只支持scp上传，为后续上传扩展留下接口
  mode: "scp",
  // 需要绝对路径
  target: path.join(__dirname, "./dist"),
  // 上传之前的提示打印
  // 在这里面你可以给出提示，用于区分环境等信息
  // 该信息会追加到当前上传文件的列表后面
  // eg：当前目录下存在[1.txt,js,main.js]文件，是否进行部署？(当前process.env.production：false)
  preHint: () => `当前process.env.production：${process.env.production}`,
  // 是否打印上传文件的log
  verbose: true,
  // 服务的配置
  // 可以接受一个函数以及一个对象
  // 如果返回的对象中不包含host/username/port/path，会提示用户进行输入
  server: () => {
    return process.env.NODE_ENV === "production"
      ? {
          // 地址
          host: "0.0.0.0",
          // 登录名称
          username: "root",
          // 密码
          password: "xxxx",
          // 端口
          port: "11024",
          // 服务端路径
          path: "/opt/docker/nginx/html/his-admin",
          // 废料存放地址，将原有的文件移到哪个文件夹下，必须保证该文件夹存在
          // 移到该文件时，会mkdir一个新文件，名称格式为：原文件夹名称+移动时间(精确到秒)，
          // 如果操作出错，可以手动根据时间将该文件进行恢复
          trashPath: "/opt/docker/nginx/html/oldhisadminfile"
        }
      : {
          host: "0.0.0.0",
          username: "root",
          password: "xxxx",
          port: "11024",
          path: "/opt/docker/nginx/html/his-admin",
          trashPath: "/opt/docker/nginx/html/oldhisadminfile"
        };
  }
};
