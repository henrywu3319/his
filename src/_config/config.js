const config = {
  publicKey: "yangjialong", //加密公钥
  //图片地址
  imageUrl: process.env.VUE_APP_IMG,
  // 请求地址
  reqUrl: {
    role: process.env.VUE_APP_ROLE,
    file: process.env.VUE_APP_FILE, //文件服务器
    hospital: process.env.VUE_APP_HOSPITAL,
    config: process.env.VUE_APP_CONFIG, // 药品配置
    user: process.env.VUE_APP_USER, // 药品配置
    order: process.env.VUE_APP_ORDER,
    child:process.env.VUE_APP_CHILD,//儿保系统
    other:process.env.VUE_APP_OTHER//讯飞
  }
};
Vue.prototype.$config = config;
export default config;
