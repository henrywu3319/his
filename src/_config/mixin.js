import utils from "@/_js/utils.js";
import Valid from '@/_js/validate'
let mixin = {
  computed: {
    adminUser() {
      let th = this;
      let user = th.$store.getters.get_adminUser;
      let j = {
        head: user.head,
        hpId: th.jieMi(user.hpId),
        memberId: th.jieMi(user.memberId),
        memberLogin: user.memberLogin,
        memberName: user.memberName,
        memberSex: user.memberSex,
        phone: th.jieMi(user.phone),
        roleId: th.jieMi(user.roleId),
        hospitalName: user.hospitalName,
        hospitalLogo: user.hospitalLogo,
        hospitalProvince: user.hospitalProvince,
        hospitalCity: user.hospitalCity,
        hospitalDistrict: user.hospitalDistrict,
        hospitalAddress: user.hospitalAddress,
        roleName: user.roleName,
        printLogo: user.printLogo,
        token: th.jieMi(user.token)
      };
      return j;
    }
  },
  created() { },
  methods: {
    /**通用类型查询 */
    async getCommonConfigByKey(key) {
      let th = this;
      return new Promise(function (s, e) {
        th.$api
          .getCommonConfigByKey({ configKey: key, systemCode: "008" })
          .then(res => {
            if (res.executed) {
              let json = JSON.parse(res.data.configValue);
              s(json);
            } else {
              e(res.message);
            }
          });
      });
    },
    //儿保保存体检信息
    saveInfoValues(data) {
      let th = this;
      let values = [];
      for (let i = 0; i < data.length; i++) {
        let arr = { attrId:data[i].id, isRequire:data[i].isRequire, attrCode: data[i].attrCode, values: "",bos:false };
        if (data[i].isRequire == "T" && data[i].values == null) {

          arr.bos = true;
        }
        if (data[i].attrType == "SELECT") {
          let aa = "";
          let b = "";
          for (let j = 0; j < data[i].attrValue.length; j++) {
            if (
              data[i].attrValue[j].valValue == data[i].values ||
              data[i].attrValue[j].valKey == data[i].values ||
              data[i].attrValue[j].id == data[i].values
            ) {
              aa += data[i].attrValue[j].valKey + ",";
              b += data[i].attrValue[j].valValue + ",";
            }
          }
          arr.values = aa.substr(0, aa.length - 1);
          arr.values2 = b.substr(0, b.length - 1);
        } else {
          arr.values = data[i].values;
        }
        values.push(arr);
      }
      let nums=values.reduce((total,it)=>{
        return it.bos==true?total+1:total+0
      },0)
      if (nums<1) {
        return values;
      } else {
        return nums>0?true:false;
      }
    },
    //获取坐标点---儿保
    getPointList(value) {
      let options = [];
      if (value != null && value != undefined) {
        for (let i = 0; i < value.length; i++) {
          let a = value[i].split(",");
          let list = [];
          for (let j = 0; j < a.length; j++) {
            list.push(Number(a[j]));
          }
          options.push(list);
        }
      }
      return options;
    },
    //接收体检信息
    getExaminInfo(info, data) {
      let infoList = []
      for (let i = 0; i < info.length; i++) {
        let arr = { values: "" ,printvalue:'',value:''};
        Object.assign(arr, info[i]);
        if(data){
          for (let j = 0; j < data.length; j++) {
            if (info[i].id == data[j].attrId) {
              if (info[i].attrType == "SELECT") {
                let attrList = info[i].attrValue;
                for (let k = 0; k < attrList.length; k++) {
                  if (attrList[k].valValue == data[j].values || attrList[k].valKey == data[j].values) {
                    arr.value = attrList[k].valKey;
                    arr.values = attrList[k].valKey;
                    arr.printvalue = attrList[k].valValue;
                    arr.values2 = attrList[k].valValue;
                  }
                }
              } else {
                let tsValue=Number(data[j].values);
                if (tsValue != "" || tsValue!=NaN ) {
                  if(tsValue>1000){
                    arr.printvalue = data[j].values2
                    arr.values = data[j].values2;
                    arr.value = data[j].values2;
                  }else{
                    arr.printvalue = data[j].values
                    arr.values = data[j].values;
                    arr.value = data[j].values;
                  }
                } else {
                  arr.values = "";
                  arr.printvalue = "";
                  arr.value ='';
                }
              }
            }
          }
        }
        infoList.push(arr);
      }
      if (infoList.length > 0) {
        return infoList;
      } else {
        return [];
      }
    },
    //添加页面缓存
    addPageCache(pageName) {
      const th = this;
      let path = th.$route.path;
      //在对应的nav里面加入缓存关键key
      let nav = th.$store.state.navMenuList.find(d => {
        return d.url == path;
      });
      if (nav) {
        th.$store.commit("add_includeCom", pageName);
        nav.sign = pageName;
      }
    },
    /**
     * 删除缓存
     */
    delCache(cacheName) {
      const th = this;
      th.$store.commit("Del_includeCom", cacheName);
    }
  }
};
const prototype = {
  /**
   * 显示加载中
   * @param bo 默认true
   */
  showLoading: function (bo = true) {
    let th = this;
    th.$store.commit("Set_vloading", bo);
  },
  /**
   * 添加导航
   * @param name 导航名称
   * @param url  跳转地址
   * @param sign  导航标识 (缓存使用)
   */
  addNavMenu(name, url, sign = "") {
    let th = this;
    th.$store.commit("Add_navMenuList", {
      name: name,
      url: url,
      sign: sign
    });
    // th.$router.push(url);
  },
  /**
   * 获取按钮权限(注意：该方法是异步，必要时需要等待)
   */
  getQX() {
    let th = this;
    let fn = function () {
      let path = th.$route.path;
      //读取是否子级path,如果存在将替换成父级path进行验证
      if (th.$route.meta.path) {
        let str = th.$route.path.substring(
          0,
          th.$route.path.lastIndexOf("/") + 1
        );
        path = str + th.$route.meta.path;
      }
      //先验证页面权限
      let vBo = th.verifyUrl(path);
      if (!vBo) {
        console.log(vBo);
        // th.$router.push("/");
        // return;
      }
      let j = {
        permissionsId: th.$store.state.qxId,
        adminRoleId: th.adminUser.roleId
      };
      th.$api.getRolePermissionButtonAndCheck(j).then(res => {
        if (res.executed) {
          res.dataBody.forEach(element => {
            th.$set(th.qxBtns, element.key, element.check);
            // th.qxBtns[element.code] = element.check;
          });
        }
      });
    };
    let t = setInterval(function () {
      if (th.$store.state.menuArray) {
        clearInterval(t);
        fn();
      }
    }, 300);
  },
  /**
   * 可以用来判断按钮链接权限
   * 设置权限Id并且判断是否有权限访问
   * @param {*} route 路由路径(不带参数)
   */
  verifyUrl(route) {
    let th = this;
    let menuData = null;
    let menuDataChild = null;
    let menuArray = th.$store.state.menuArray;
    for (let i = 0; i < menuArray.length; i++) {
      let d = menuArray[i];
      if (d.url == route) {
        menuData = d;
        break;
      }
      //读取下面的子级
      if (d.child && d.child.length > 0) {
        for (let g = 0; g < d.child.length; g++) {
          let d2 = d.child[g];
          if (d2.url == route) {
            menuData = d;
            menuDataChild = d2;
            break;
          }
        }
        if (menuDataChild) {
          break;
        }
      }
    }
    let p = menuDataChild || menuData;

    if (p) {
      //设置权限
      th.$store.commit("Set_qxId", p.permissionId);
      return true;
    } else {
      th.$message.error("您被拒绝访问！");
      return false;
    }
  },
  /**
   * 加密
   */
  jiaMi(value) {
    return this.$utils.stringToEncryption(value);
  },
  /**
   * 解密
   */
  jieMi(value) {
    return this.$utils.stringToDecode(value);
  },
  /**
   * 生产url安全token
   */
  getUrlToken(json) {
    return this.$utils.createSafetyParam(json);
  },
  /**
   * 验证Url是否被串改
   */
  verifyUrlAttack(isBack = true) {
    let th = this;
    return new Promise(function (resolve, reject) {
      th.$utils
        .validSafetyParam(th.$route.query)
        .then(() => {
          resolve();
        })
        .catch(() => {
          th.$message.error("检测到非法进入");
          if (isBack) th.$router.push("/");
          reject();
        });
    });
  },
  //g获取时间--01号
  getDate(type) {
    let date_str = "";
    let date = new Date();
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    if (m < 10) {
      m = "0" + m;
    }
    let d = date.getDate();
    if (d < 10) {
      d = "0" + d;
    }
    if (type !== 1) {
      d = "01";
    }
    date_str = y + "-" + m + "-" + d;
    return date_str;
  },
  getNewDate() {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    if (month < 10) {
      month = "0" + month;
    }
    if (day < 10) {
      day = "0" + day;
    }
    return year + "-" + month + "-" + day;
  },
  //获取儿童体检日期
  getChildDate() {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    if (month < 10) {
      month = "0" + month;
    }
    if (day < 10) {
      day = "0" + day;
    }
    return year + "/" + month + "/" + day;
  }
};
Vue.mixin(mixin);
for (let key in prototype) {
  Vue.prototype[key] = prototype[key];
}
Vue.prototype.$utils = utils;
