import http from "@/_js/http.js";
let tApi = {
  //============================患者
  /** 查询患者所有信息（护士工作台） */
  patientnurseWorkbenchlist(json) {
    return http.Get("config/api/patient/nurseWorkbench/list", json);
  },
  /** 查询患者列表 */

  getThePatientLibraryList(json) {
    return http.Get("user/patient/getThePatientLibraryList", json);
  },
  //患者详情
  getThePatientLibraryById(json) {
    return http.Get("user/patient/getThePatientLibraryById", json);
  },
  //患者来源
  getPatientSource(json) {
    return http.Get("user/patientSource/getPatientSource", json);
  },
  //删除患者
  deletePatient(json) {
    return http.Post("user/patient/deletePatient", json);
  },
  //添加患者
  addPatient(json) {
    return http.Post("user/patient/addPatient", json);
  },
  //获取会员列表
  selectLevelEnumList(json) {
    return http.Get("user/levelEnum/selectLevelEnumList", json);
  },
  //设置会员
  updateLevelEnumList(json) {
    return http.Post("user/levelEnum/updateLevelEnumList", json);
  },
  //获取会员列表
  LevelList(json) {
    return http.Get("user/levelEnum/selectMemberTypePage", json);
  },
  //查询患者档案信息
  getPatientDetail(json) {
    return http.Get("config/api/patient/detail", json);
  },
  //获取用户未签署的知情同意书列表
  getInformedConsent(json) {
    return http.Get("user/informedConsent/getInformedConsent", json);
  },
  //签署知情同意书
  signInformedConsent(json) {
    return http.Post("user/informedConsent/signInformedConsent", json, true);
  },
  //获取患者签名记录列表
  getSignInformedConsent(json) {
    return http.Get("user/informedConsent/getSignInformedConsent", json);
  },
  //================------====患者体征
  //查询单个
  getPatientSignById(json) {
    return http.Get("user/patientSign/getPatientSignById", json);
  },
  //保存体检信息
  savePatientSign(json) {
    return http.Post("user/patientSign/savePatientSign", json, true);
  },
  //保存血糖范围
  savePatientGlucosePrice(json) {
    return http.Post("user/patientSignRecord/savePatientGlucosePrice", json, true);
  },
  //查询血糖范围
  gePatientGlucosePriceById(json) {
    return http.Get("user/patientSignRecord/gePatientGlucosePriceById", json, true);
  },
  //保存血糖记录
  savePatientGlucoseRecord(json) {
    return http.Post("user/patientSignRecord/savePatientGlucoseRecord", json, true);
  },
  //血糖记录
  selectPatientGlucoseRecord(json) {
    return http.Get("user/patientSignRecord/selectPatientGlucoseRecord", json, true);
  },
  //========================患者过敏
  //查询患者过敏信息
  getAllergyById(json) {
    return http.Get("user/patientAllergy/getAllergyById", json);
  },
  //修改患者过敏
  saveAllergyAndHistory(json) {
    return http.Post("user/patientAllergy/saveAllergy", json, true);
  },
  //查询过敏信息
  pageQueryMedicalStock(json) {
    return http.Get("config/api/allergyEnum/pageQueryMedicalStock", json);
  },
  //添加过敏
  addAllergyEnum(json) {
    return http.Post("config/api/allergyEnum/addAllergyEnum", json, true);
  },
  //========================患者病案
  //查询单个患者历史信息
  getPatientHistoryById(json) {
    return http.Get("user/patientHistory/getHistoryById", json);
  },
  //保存个人历史信息
  patientHistorySaveHistory(json) {
    return http.Post("user/patientHistory/saveHistory", json, true);
  },
  //获取标签
  getLabelEmunByIds(json) {
    return http.Post("user/labelEmun/getLabelEmunByIds", json, true);
  },
  //========================患者家庭成员
  //查询患者家庭成员列表
  getPatientRelationList(json) {
    return http.Get("user/patientFamily/getPatientRelationList", json, true);
  },
  //添加家庭成员
  addPatientFamily(json) {
    return http.Post("user/patientFamily/addPatientFamily", json, true);
  },
  //修改家庭成员
  updatePatientFamily(json) {
    return http.Post("user/patientFamily/updatePatientFamily", json, true);
  },
  //删除家庭成员
  deletePatientRelation(json) {
    return http.Post("user/patientFamily/deletePatientRelation", json);
  },
  // //保存个人过敏信息
  // saveAllergy(json) {
  //   return http.Post("user/patientAllergy/saveAllergy", json, true);
  // },
  //获取标签
  selectLabelEmunList(json) {
    return http.Get("user/labelEmun/selectLabelEmunList", json);
  },
  //保存体征记录
  savePatientSignRecord(json) {
    return http.Post("user/patientSignRecord/savePatientSignRecord", json);
  },
  //获取体征记录详情
  getPatientSignRecordById(json) {
    return http.Post("user/patientSignRecord/getPatientSignRecordById", json);
  },
  //=============出入库 ---tl
  //获取入库信息
  getMedicalStockList(json) {
    return http.Get("config/api/medicalStock/pageQueryMedicalStock", json);
  },
  //获取药品列表
  pageQueryMedicalDrugInfo(json) {
    return http.Get("config/api/medicalDrug/pageQueryMedicalDrugInfo", json);
  },
  //查询药品信息
  findMedicalDrugById(json) {
    return http.Get("config/api/medicalDrug/findMedicalDrugById", json);
  },
  //查询药品用法详细信息
  findMedicalDrugUseByDrugId(json) {
    return http.Get("config/api/medicalDrugUse/findMedicalDrugUseByDrugId", json);
  },
  //添加入库---军
  addMedicalStock(json) {
    return http.Post("config/api/medicalStock/addMedicalStock", json);
  },
  //添加入库
  addMedicalStockV2(json) {
    return http.Post("config/api/medicalStock/addMedicalStockV2", json);
  },
  //修改入库
  updateMedicalStock(json) {
    return http.Post("config/api/medicalStock/updateMedicalStock", json);
  },
  //修改入库V2
  updateMedicalStockV2(json) {
    return http.Post("config/api/medicalStock/updateMedicalStockV2", json);
  },
  //获取出入库信息
  findMedicalStockById(json) {
    return http.Get("config/api/medicalStock/findMedicalStockById", json);
  },
  //获取出入库信息
  findMedicalStockByIdV2(json) {
    return http.Get("config/api/medicalStock/findMedicalStockByIdV2", json);
  },
  //审核出入库
  examineStock(json) {
    return http.Get("config/api/medicalStock/examineStock", json);
  },
  //审核出入库V2
  examineStockV2(json) {
    return http.Get("config/api/medicalStock/examineStockV2", json);
  },
  //获取供应商
  findHpSupplierAll(json) {
    return http.Get("hospital/hpSupplier/findHpSupplierAll", json);
  },
  //分页查询库存
  pageStockBatch(json) {
    return http.Get("config/api/medicalStock/pageStockBatch", json);
  },
  //分页查询库存V2
  pageStockBatchV2(json) {
    return http.Get("config/api/medicalStock/pageStockBatchV2", json);
  },
  //充值
  rechargeRecord(json) {
    return http.Post("order/rechargeRecord/recharge", json);
  },
  //退款
  rechargeReturn(json) {
    return http.Post("order/rechargeRecord/rechargeReturn", json);
  },
  //会员id
  getLevelEnumById(json) {
    return http.Get("user/levelEnum/getLevelEnumById", json);
  },
  //充值记录列表
  getRechargeRecord(json) {
    return http.Get("order/rechargeRecord/getRechargeRecord", json);
  },
  //积分列表
  getIntegralRecord(json) {
    return http.Get("order/integralRecord/getIntegralRecord", json);
  },
  //=============收费
  //收费记录
  getOrderDayReport(json) {
    return http.Get("order/orderRecord/findOrderRecordAll", json);
  },
  //退费
  getRefundRecord(json) {
    return http.Get("order/orderRefund/getRefundRecord", json);
  },
  //退费详情
  getRefimdRecordDetail(json) {
    return http.Get("order/orderRefund/getRefundRecordDetail", json);
  },
  //收费模板-待收费
  findNoPayList(json) {
    return http.Get("config/api/cure/findNoPayList", json);
  },
  //=============药品出入库
  //查盘点
  pageQueryMedicalInventory(json) {
    return http.Get("config/api/medicalInventory/pageQueryMedicalInventory", json);
  },
  //添加盘点数据
  addMedicalInventory(json) {
    return http.Post("config/api/medicalInventory/addMedicalInventory", json);
  },
  //添加盘点数据V2
  addMedicalInventoryV2(json) {
    return http.Post("config/api/medicalInventory/addMedicalInventoryV2", json);
  },
  //查找盘点详情---info
  findMedicalInventoryById(json) {
    return http.Get("config/api/medicalInventory/findMedicalInventoryById", json);
  },
  //查看修改盘点数据
  findItemBatchByChangeInventoryId(json) {
    return http.Get("config/api/medicalInventory/findItemBatchByChangeInventoryId", json);
  },
  //修改盘点数据
  updateMedicalInventory(json) {
    return http.Post("config/api/medicalInventory/updateMedicalInventory", json);
  },
  //修改盘点数据
  updateMedicalInventoryV2(json) {
    return http.Post("config/api/medicalInventory/updateMedicalInventoryV2", json);
  },
  //盘点详情---list
  findMedicalInventoryItemPage(json) {
    return http.Get("config/api/medicalInventory/findMedicalInventoryItemPage", json);
  },
  //盘点详情V2---list
  findMedicalInventoryItemPageV2(json) {
    return http.Get("config/api/medicalInventory/findMedicalInventoryItemPageV2", json);
  },
  //审核盘点
  examinInventory(json) {
    return http.Get("config/api/medicalInventory/examineStock", json);
  },
  //审核盘点V2
  examinInventoryV2(json) {
    return http.Get("config/api/medicalInventory/examineStockV2", json);
  },
  //删除出入库
  delMedicalStock(json) {
    return http.Post("config/api/medicalStock/delMedicalStock", json);
  },
  //-----------------物资
  //物资收费
  pageQueryMaterialFee(json) {
    return http.Get("config/api/materialFee/pageQueryMaterialFee", json);
  },
  //==========积分
  //修改积分
  updateRecord(json) {
    return http.Post("order/integralRecord/updateRecord", json);
  },
  //查看记录
  getIntegralRecord(json) {
    return http.Get("order/integralRecord/getIntegralRecord", json);
  },
  //零售药品生成订单
  firstPayOrder(json) {
    return http.Post("order/order/firstPayOrder", json);
  },
  //获取订单
  getOrder(json) {
    return http.Get("order/order/getOrder", json);
  },
  //获取订单详情
  getOrderItem(json) {
    return http.Get("order/order/getOrderItem", json);
  },
  //订单退款
  payReturnOrder(json) {
    return http.Post("order/order/payReturnOrder", json);
  },
  //收费订单详情
  findNoPayDetailList(json) {
    return http.Get("config/api/cure/findNoPayDetailList", json);
  },
  //收费订单详情--2019.11.14---彭
  findNoPayDetailListByNew(json) {
    return http.Get("config/api/cure/findNoPayDetailListV2", json);
  },
  //生成订单
  createOrder(json) {
    return http.Post("order/order/createOrder", json);
  },
  //取消订单
  cancelOrder(json) {
    return http.Post("order/order/cancelOrder", json);
  },
  //结账----存在订单结账
  payOrder(json) {
    return http.Post("order/order/payOrder", json);
  },
  //获取积分规则
  getIntegralRuleById(json) {
    return http.Get("user/integralRule/getIntegralRuleById", json);
  },
  //获取还账记录
  getReturnDiscount(json) {
    return http.Get("order/discountRecord/getReturnDiscount", json);
  },
  //获取挂账记录
  getDiscount(json) {
    return http.Get("order/discountRecord/getDiscount", json);
  },
  //还账
  returnDiscount(json) {
    return http.Post("order/discountRecord/returnDiscount", json);
  },
  //生成小程序验证码-----2019.11.19  陈
  createSmallAppCode(json) {
    return http.Post("order/order/createSmallAppCode", json);
  },
  //======================统计
  //收费日报
  getOrderDayReportByChart(json) {
    return http.Get("order/report/getOrderDayReport", json);
  },
  //收费月报
  getOrderMonthReport(json) {
    return http.Get("order/report/getOrderMonthReport", json);
  },
  //储值卡余额统计
  selectPatientBalance(json) {
    return http.Get("user/patientHospital/selectPatientBalance", json);
  },
  //收费明细
  getOrderItemDetail(json) {
    return http.Get("order/report/getOrderItemDetail", json);
  },
  //获取挂账记录
  getDiscountRecord(json) {
    return http.Get("order/discountRecord/getDiscount", json);
  },
  //获取还账记录
  getReturnDiscountRecord(json) {
    return http.Get("order/discountRecord/getReturnDiscount", json);
  },
  //医生接诊
  doctorReception(json) {
    return http.Get("config/api/report/medical/doctorReception", json);
  },
  //就诊类型
  receptionType(json) {
    return http.Get("config/api/report/medical/receptionType", json);
  },
  //患者来源
  selectPatientsWithSourceList(json) {
    return http.Get("user/patientSource/selectPatientsSourceCountList", json);
  },
  //患者来源V2
  selectPatientsSourcePageV2(json) {
    return http.Get("user/patientSource/selectPatientsSourcePage", json);
  },
  //来源类型推荐统计
  getPatientsSourceCount(json) {
    return http.Get("user/patientSource/getPatientsSourceCount", json);
  },
  //收费分类
  getPaywayReport(json) {
    return http.Get("order/report/getPaywayReport", json);
  },
  //===============其他
  //获取语音合成
  xfApiCreateMusicByText(json) {
    return http.Get("other/xfApi/xfApiCreateMusicByText", json);
  },
  //============北妇叫号
  //诊室叫号列表
  getCallOrderListByRoom(json) {
    return http.Get("hospital/callOrder/getCallOrderListByRoom", json);
  },
  //开始叫号
  callOrderSingle(json) {
    return http.Post("hospital/callOrder/callOrderSingle", json);
  },
  //======lis
  //检查报告
  findCheckListByLis(json) {
    return http.Post("config/pui/lis/findCheckList", json);
  },
  //获取检查详情
  findCheckDetailById(json) {
    return http.Post("config/pui/lis/findCheckDetail", json);
  },
  //获取患者辅助检查详情
  getCureAssistDetailById(json) {
    return http.Get("config/api/cureAssist/list/patientId", json);
  },
  //===============儿保================
  //查询儿童体检类目信息
  findListByCategoryId(json) {
    return http.Post("child/attr/findListByCategoryIdGx", json);
  },
  //保存儿童体格检查
  insertPhysicalExamination(json) {
    return http.Post("child/physicalExamination/saveValuesGx", json);
  },
  //获取体检次数
  getExaminationNums(json) {
    return http.Post("child/physicalExamination/findListGx", json);
  },
  //获取儿童基础档案
  getChildInfo(json) {
    return http.Post("child/childArchives/findIdGx", json);
  },
  //保存儿保基础信息
  insertBasicInfo(json) {
    return http.Post("child/childArchives/insertValuesGx", json);
  },
  //添加儿童档案
  insertChildArchives(json) {
    return http.Post("child//childArchives/insertGx", json);
  },
  //根据患者ID判断是否有档案
  getArchivesById(json) {
    return http.Post("child/childArchives/findByPatientId", json);
  },
  //WHO生长曲线图
  getChildWho(json) {
    return http.Post("child/who/findCountInfoByArchivesIdGx", json);
  },
  findChildTips(json) {
    return http.Post("child/physicalExamination/findTips", json);
  },
  //====================新接口  12.16
  //关联健康卡号
  patientRelevanceCard(json) {
    return http.PostJson("config/api/patientHealthCard/relate-patient-by-card-num-secret", json);
  },
  //获取体检月龄
  getMonthByExamination(json) {
    return http.Post("child/childArchives/getAgeMonth", json);
  },
  //添加自带检查报告
  addPatientReport(json) {
    return http.Post("config/api/patient-private-report/add", json);
  },
  //获取患者自带辅助检查报告详情
  getPatientReportById(json) {
    return http.Post("config/api/patient-private-report/detail/id", json);
  },
  //修改自带检查报告
  editPatientReport(json) {
    return http.Post("config/api/patient-private-report/update", json);
  },
  //2020.01.06开始====
  //更新批次信息----2020.01.06
  editStockDetail(json) {
    return http.PostJson("config/api/medicalStock/update-stock-batch", json);
  },
  //出库V2
  getPutStorge(json) {
    return http.PostJson("config/api/medicalStock/page-query-medical-stock-out-put", json);
  },
  //出库详情V2
  getPutDetailV2(json) {
    return http.Post("config/api/medicalStock/medical-stock-out-put-detail", json);
  },
  //添加出库V2
  addPutStorge(json) {
    return http.PostJson("config/api/medicalStock/medical-stock-out-put", json);
  },
  //修改出库V2
  editPutStorge(json) {
    return http.PostJson("config/api/medicalStock/medical-stock-out-put/update", json);
  },
  //审核出库
  examinePutStorge(json) {
    return http.Get("config/api/medicalStock/stock-out-put-examine", json);
  },
  //取消库存
  cancelStock(json) {
    return http.Get("config/api/medicalStock/cancel-stock", json);
  },
  /**获取用户套餐树 */
  getUserTreeList(json) {
    return http.Get("user/userPackageProject/userPackage/treeList", json);
  },
  /**获取来源 */
  patientSourceStatistics(json) {
    // return http.Get("user/patientSource/patientSourceStatistics",json);
    return http.Get("order/order/getPatientSourceStatistics", json);
  },
  /**机构来源查询 */
  findOrganizationAll(json) {
    return http.Get("user/organization/findOrganizationAll", json);
  },
  /**============ 科室领药 2020.06.09 ============== */
  //科室领药记录列表
  getMedicalDrugApplyLog(json) {
    return http.Get("config/api/drugApply/pageQuery", json);
  },
  //添加领药记录
  addDrugApplyLog(json) {
    return http.Post("config/api/drugApply/addDrugApplyLog", json);
  },
  //取消申请
  cancelApply(json) {
    return http.Post("config/api/drugApply/cancelApply", json);
  },
  //确认出库
  confirmOutbound(json) {
    return http.Post("config/api/drugApply/confirmOutbound", json);
  },
  //拒绝出库
  rejectOutbound(json) {
    return http.Post("config/api/drugApply/rejectOutbound", json);
  },
  //申请记录详情---医生视角
  getDrugApplyLogById(json) {
    return http.Get("config/api/drugApply/getDrugApplyLogById", json);
  },
  //科室申领审核详情
  getOutboundAuditList(json) {
    return http.Get("config/api/drugApply/getOutboundAuditList", json);
  },
  //根据memberId获取信息-医生
  getMemberInfo(json) {
    return http.Get("hospital/member/getMemberInfoByIds", json);
  },
  //判断是否有诊疗记录
  checkPatientIsExistCureRecord(json) {
    return http.Post("config/api/cure/checkPatientIsExistCureRecord", json);
  },
  //获取会员信息
  getPatientMemberInfo(json) {
    return http.Get("user/patientAccount/getPatientMemberInfo", json);
  },
  //退费---2020.08.04新增
  rechargeCenterRefund(json) {
    return http.Post("user/rechargeCenter/refund", json);
  },
  //充值
  rechargeCenterTopUp(json) {
    return http.Post("user/rechargeCenter/topUp", json);
  },
  //充值记录
  rechargeRecordList(json) {
    return http.Get("order/rechargeRecord/rechargeRecord", json);
  },
  //会员-退款详情
  refundDetail(json) {
    return http.Get("user/rechargeCenter/refundDetail", json);
  },
  //患者详情
  getPatientInfo(json) {
    return http.Get("user/patient/getPatientInfo", json);
  },
  /**
   *========================= 随访V2  -  2020.09.09   -by 谢飞-接口==========================
   */
  /**
   * 获取随访记录
   */
  getVisitRecordByPage(json) {
    return http.Get("config/api/visit_record/page", json);
  },
  /**
   * 新增/修改随访
   */
  addOrEditVisit(json) {
    return http.Post("config/api/visit_record/add_or_update", json);
  },
  /**
   * 获取随访项目类型列表
   */
  getVisitTypeList(json) {
    return http.Get("config/api/visit_type/list", json);
  },
  /**
   * 获取随访项目
   */
  getAllProjectList(json) {
    return http.Get("config/api/visit_project/list", json);
  },
  /**
   *获取随访记录详情
   */
  getVisitRecordDetail(json) {
    return http.Get("config/api/visit_record/get_visitRecord", json);
  },
  /**
   * 取消随访
   */
  cancelVisitRecord(json) {
    return http.Post("config/api/visit_record/cancel_visit", json);
  },
  /**
   * 查询随访结果
   */
  getVisitResult(json) {
    return http.Get("config/api/visit_result/get_result", json);
  },
  /**
   * 保存随访结果
   */
  addOrUpdateVisitResult(json) {
    return http.Post("config/api/visit_result/add_or_update", json);
  },
  /**
   * 随访统计
   */
  getVisitStatistics(json) {
    return http.Get("config/api/visit_record/statistics", json);
  },
  /**
   * 随访状态菜单栏
   */
  getVisitStatusList(json) {
    return http.Get("config/api/visit_record/status_count_list", json);
  },
  /**
   * 随访项目导出
   */
  getVisitResultExport(json) {
    return http.Get("config/api/visit_result/export", json);
  },
  /*--------------- 收费V2   ---------- */
  /**
   * 待收费列表
   */
  getNoPayCureList(json) {
    return http.Get("config/api/cure/getNoPayCureList", json);
  },
  //待收费详情
  getNoPayCureDetailList(json) {
    return http.Get("config/api/cure/getNoPayCureDetailList", json);
  },
  //创建订单
  createNewOrder(json) {
    return http.Post("order/medical-order/create-order", json);
  },
  //收费列表
  getMedicalOrderList(json) {
    return http.Get("order/medical-order/get-order", json);
  },
  //取消订单
  cancelOrderV2(json) {
    return http.Post("order/medical-order/cancel-order", json);
  },
  //支付订单
  payOrder(json) {
    return http.Post("order/medical-order/pay-order", json);
  },
  //获取订单详情
  getOrderDetailByMedical(json) {
    return http.Get("order/medical-order/get-order-detail", json);
  },
  //订单退款
  refundOrder(json) {
    return http.Post("order/medical-order/refund-order", json);
  },
  //退款列表
  getOrderRefundList(json) {
    return http.Get("order/medical-order/get-order-refund", json);
  },
  //退款订单详情
  getOrderRefundDetail(json) {
    return http.Get("order/medical-order/get-order-refund-detail", json);
  },
  //套餐详情
  getPackageUseDetail(json) {
    return http.Get("order/medical-order/get-package-use-detail", json);
  },
  //患者收费信息列表
  getOrderRecordList(json) {
    return http.Get("order/medical-order/order-record", json);
  },
  //=======   2020.12.16新增接口  ========
  //取消实验室检查
  cancelCureCheckup(json) {
    return http.Post("config/api/cureCheckup/cancel", json);
  },
  //======    医生门诊查询报告  ===========
  getPatientReportDate(json) {
    return http.Get("config/api/cure/patientReportDate", json);
  },
  /**
   * 实验室检查标签
   */
  getPrintInformation(json) {
    return http.Get("config/api/cureCheckup/confirmedCheck/getPrintInformation", json);
  },
  /**
   * 删除套餐项目
   */
  deleteSourceIdByCombo(json) {
    return http.Post("config/api/medical-combo/v2/detail/deleteSource", json);
  }
};
export { tApi };
