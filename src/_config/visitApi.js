// 随访api
import http from "@/_js/http.js";
export default {
  /** 随访状态统计数据 */
  followUpVisitstatisticstatus(json) {
    return http.Get("config/api/followUpVisit/statistic/status", json);
  },
  /** 随访类型统计数据 */
  followUpVisitstatisticvisitType(json) {
    return http.Get("config/api/followUpVisit/statistic/visitType", json);
  },
  /** 分页查询随访列表 */
  followUpVisitpageQuery(json) {
    return http.Get("config/api/followUpVisit/pageQuery", json);
  },
  /** 分页查询随访类容模板数据 */
  followUpVisitContentTemplatepageQuery(json) {
    return http.Get("config/api/followUpVisitContentTemplate/pageQuery", json);
  },
  /** 分页查询随访类型数据 */
  followUpVisitTypepageQuery(json) {
    return http.Get("config/api/followUpVisitType/pageQuery", json);
  },
  /** 添加随访数据 */
  addfollowUpVisit(json) {
    return http.Post("config/api/followUpVisit/add", json, true);
  },
  /** 添加随访周期数据 */
  addfollowUpVisitPeriod(json) {
    return http.Post("config/api/followUpVisitPeriod/add", json, true);
  },
  /** 获取患者随访周期信息 */
  getfollowUpVisitPeriod(json) {
    return http.Get("config/api/followUpVisitPeriod/get", json);
  },
  /** 已随访详情 */
  followUpVisitdetailvisited(json) {
    return http.Get("config/api/followUpVisit/detail/visited", json);
  },
  /** 随访页面随访内容查询 */
  followUpVisitcontentid(json) {
    return http.Get("config/api/followUpVisit/detail/content/id", json);
  },
  /** 随访详情 */
  followUpVisitvisitdetail(json) {
    return http.Get("config/api/followUpVisit/detail/visit", json);
  },
  /** 更新随访数据 */
  updatefollowUpVisit(json) {
    return http.Post("config/api/followUpVisit/update", json, true);
  },
  /** 随访结果填写 */
  addvisitResult(json) {
    return http.Post("config/api/followUpVisit/add/visitResult", json, true);
  },
  /** 分页查询随访结论模板数据 */
  followUpVisitResultTemplatepageQuery(json) {
    return http.Get("config/api/followUpVisitResultTemplate/pageQuery", json);
  },
  /** 随访类型详情(仅返回选择的项) */
  followUpVisitTypechoicedetail(json) {
    return http.Get("config/api/followUpVisitType/choice/detail", json);
  },
  /** 随访结论模板详情 */
  followUpVisitResultTemplatedetail(json) {
    return http.Get("config/api/followUpVisitResultTemplate/detail", json);
  },
  /** 随访结果填写 */
  followUpVisitaddvisitResult(json) {
    return http.Post("config/api/followUpVisit/add/visitResult", json, true);
  },
  /** 取消随访 */
  followUpVisitcancel(json) {
    return http.Post("config/api/followUpVisit/cancel/id", json, true);
  },
  /** 取消随访详情 */
  cancelvisitContentdetail(json) {
    return http.Get(
      "config/api/followUpVisit/detail/cancel/visitContent",
      json
    );
  },
  
};
