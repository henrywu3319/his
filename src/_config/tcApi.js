//套餐接口（新）
import http from "@/_js/http.js";
export default {
  //获取全部用户拥有套餐项目数据
  tc_findUserPackageProjectAll(json) {
    return http.Get("user/userPackageProject/findUserPackageProjectAll", json);
  },
  //获取全部用户拥有套餐二级项目数据
  tc_findUserPackageProjectItemAll(json) {
    return http.Get("user/userPackageProject/findUserPackageProjectItemAll", json);
  },
  //获取全部用户拥有套餐子项数据
  tc_findUserPackageProjectItemDetailAll(json) {
    return http.Get("user/userPackageProject/findUserPackageProjectItemDetailAll", json);
  }, 
   //添加用户拥有套餐项目数据-未支付
   tc_addUserPackageProjectNoPay(json) {
    return http.Post("user/userPackageProject/addUserPackageProjectNoPay", json,true);
  }, 
  //套餐子项详情查询
  tc_sourceandsource_id(json) {
    return http.Post("config/api/medical-combo/v2/detail/source-and-source-id", json);
  }, 
  //添加门诊套餐记录数据
  tc_curePackageRecordadd(json) {
    return http.Post("config/api/curePackageRecord/add", json,true);
  }, 
   //将用户未支付的套餐取消
  tc_cancelUserPackageProjectNoPay(json) {
    return http.Post("user/userPackageProject/cancelUserPackageProjectNoPay", json,true);
  }, 
   //取消门诊套餐记录
  tc_curePackageRecordcurecancel(json) {
    return http.Get("config/api/curePackageRecord/cure/cancel", json);
  }
};
