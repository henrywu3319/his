import http from "@/_js/http.js";
import { yApi } from "./yApi.js";
import { tApi } from "./tApi";
import visitApi from "./visitApi.js";
import tcApi from "./tcApi.js";
//请在_config文件夹下配置地址
let api = {
  /** 获取省市区 */
  findCoreRegionAll(keywords, adcode, subdistrict = 1) {
    let j = {
      key: "d8d0efbfbc4c42d120280cc8616276b1",
      subdistrict: subdistrict,
      keywords: keywords,
      filter: adcode,
      addHead: false
    };
    return http.Get("https://restapi.amap.com/v3/config/district", j);
  },
  /** 获取oss临时凭证 */
  getTemporaryAKAndToken(sessionName) {
    return http.Get("role/comment/getTemporaryAKAndToken", {
      sessionName: sessionName,
      addHead: false
    });
  },
  //获取医院byID
  getHospitalById(json) {
    return http.Get("role/hpHospital/getHospital", json);
  },
  //获取配置查询
  getCommonConfigByKey(json) {
    return http.Get("config/api/commonConfig/getCommonConfigByKey", json);
  },
  //登录(获取角色)
  getRoles(json) {
    return http.Post("role/member/getLoginMemberRoles", json, false);
  },
  /** 获取登录用户信息 */
  getMemberInfoByToken() {
    return http.Get("role/member/getMemberInfoByToken", {});
  },
  /** 登录 */
  login(json) {
    return http.Post("role/member/login", json);
  },
  /**修改密码 */
  updatePwd(json) {
    return http.Post("role/member/updateMemberPwd", json, true);
  },
  /**获取菜单 */
  getRolePermissions(json) {
    return http.Get(
      "role/getPermissionRelatedController/getRolePermissions",
      json
    );
  },
  /**获取人员 */
  getMemberList(json) {
    return http.Get("role/member/getMemberList", json);
  },
  /**获取按钮权限 */
  getRolePermissionButtonAndCheck(json) {
    return http.Get(
      "role/getPermissionRelatedController/getRolePermissionButtonAndCheck",
      json
    );
  },
  /**二维码和条形码 */
  findCureCode(json) {
    return http.Get("config/api/cure/findCureCode", json);
  }
};
Object.assign(api, yApi, tApi,visitApi,tcApi);
Vue.prototype.$api = api;
