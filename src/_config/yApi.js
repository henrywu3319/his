import http from "@/_js/http.js";
let yApi = {
  //============================科室
  /** 查询 */
  getDepartmentPageList(json) {
    return http.Get("hospital/hpDepartment/getDepartmentPageList", json);
  },
  //=====================诊室
  /** 查询 */
  findHpDiagnosisRoomAll(json) {
    return http.Get("hospital/hpDiagnosisRoom/findHpDiagnosisRoomAll", json);
  },
  //=========================================排班
  /** 医生列表-日期科室有排班的列表 */
  getDoctorListByDate(json) {
    return http.Get("hospital/schedule/getDoctorListByDate", json);
  },
  /** 取消医生排班 */
  canCelschedule(json) {
    return http.Post("hospital/schedule/canCelschedule", json, true);
  },
  /** 查询医生号源占用情况 */
  getSourceInfoByDoctorAbout(json) {
    return http.Get("hospital/schedule/getSourceInfoByDoctorAbout", json);
  },
  /** 获取某天有有排班医生的医生列表 */
  getDoctorScheduleListByDate(json) {
    return http.Get("hospital/register/getDoctorScheduleListByDate", json);
  },
  /** 北妇专用接口-登记或激活预约 */
  registeraddOrderSingle(json) {
    return http.Post("hospital/register/addOrderSingle", json, true);
  },
  /** 北妇查询患者 */
  usUserVisitselectVisitInfoNew(json) {
    return http.Get("http://123.57.8.156:8061/usUserVisit/selectVisitInfoNew", json);
  },
  /** 删除号源 */
  delSources(json) {
    return http.Post("hospital/schedule/delSources", json, true);
  },
  /** add号源 */
  addSource(json) {
    return http.Post("hospital/schedule/addSource", json, true);
  },
  /** 查询号源 */
  getSourcesList(json) {
    return http.Get("hospital/schedule/getSourcesList", json);
  },
  /** 查询 */
  scheduleList(json) {
    return http.Get("hospital/schedule/scheduleList", json);
  },
  /** 查询排班类型 */
  getScheduleTypeList(json) {
    return http.Get("hospital/schedule/type/list", json);
  },
  /** add排班类型 */
  addScheduleType(json) {
    return http.Post("hospital/schedule/type/add", json, true);
  },
  /** update排班类型 */
  updateScheduleType(json) {
    return http.Post("hospital/schedule/type/update", json, true);
  },
  /** 设置排班 */
  setScheduleSchedule(json) {
    return http.Post("hospital/schedule/schedule", json, true);
  },
  /** 复制排班 */
  scheduleCopy(json) {
    return http.Post("hospital/schedule/scheduleCopy", json, true);
  },
  /** 查询医生某月排班*/
  getDoctorScheduleByMonth(json) {
    return http.Get("hospital/schedule/getDoctorScheduleByMonth", json);
  },
  /** 查询医生的号源*/
  getDoctorSourceByDay(json) {
    return http.Get("hospital/schedule/getDoctorSourceByDay", json);
  },

  //======================医生
  /** 查询 */
  getDoctorById(json) {
    return http.Get("hospital/doctor/getDoctor", json);
  },
  /** 查询 */
  getDoctorPageList(json) {
    return http.Get("hospital/doctor/getDoctorPageList", json);
  },
  /** 查询单个 */
  getDoctorByToken() {
    return http.Get("hospital/doctor/getDoctorByToken", {});
  },
  /** 结束就诊 */
  closeCure(json) {
    return http.Post("config/api/cure/closeCure", json, true);
  },
  /** 门诊套餐结束就诊 */
  closeCureCombo(json) {
    return http.Post("config/api/cure/closeCureCombo", json, true);
  },
  /** 医生工作台统计 */
  getRegisterCount(json) {
    return http.Get("hospital/register/getRegisterCount", json);
  },
  /** 查询所有医生预约统计 */
  getdoctorsRegisterList(json) {
    return http.Get("hospital/schedule/doctorsRegisterList", json);
  },

  //==============================================预约
  //其他快速登记专用
  /** 导诊台 登记或激活 */
  addOrderBySourceIdOnline(json) {
    return http.Post("hospital/register/addOrderBySourceIdOnline", json, true);
  },
  /** 导诊台 登记或激活 */
  addOrderSingleOnline(json) {
    return http.Post("hospital/register/addOrderSingleOnline", json, true);
  },
  //其他快速登记专用
  /** 登记到指定号源 */
  addOrderBySourceId(json) {
    return http.Post("hospital/register/addOrderBySourceId", json, true);
  },
  /** 过号 */
  callOrderpassOrder(json) {
    return http.Get("hospital/callOrder/passOrder", json);
  },
  /** 叫号/取消 */
  callOrdercallOrder(json) {
    return http.Post("hospital/callOrder/callOrder", json, true);
  },
  /** 查询叫号患者 */
  getDoctorRegisterList(json) {
    return http.Get("hospital/callOrder/getDoctorRegisterList", json);
  },
  /** 叫号置顶 */
  callOrdertopOrder(json) {
    return http.Get("hospital/callOrder/topOrder", json, true);
  },
  /** 激活过号 */
  callOrderactivePassOrder(json) {
    return http.Get("hospital/callOrder/activePassOrder", json, true);
  },
  /** 查询日历 */
  getRegisterCalendarInfo(json) {
    return http.Get("hospital/register/getRegisterCalendarInfo", json);
  },

  /** 查询列表 */
  getRegisterListInfo(json) {
    return http.Get("hospital/register/getRegisterListInfo", json);
  },
  /** 分诊 */
  registerTriage(json) {
    return http.Post("hospital/register/triage", json, true);
  },
  /** 添加预约*/
  registerAddOrder(json) {
    return http.Post("hospital/register/addOrder", json, true);
  },
  /** 取消预约*/
  registerCancelOrder(json) {
    return http.Post("hospital/register/cancelOrder", json, true);
  },
  /** 修改预约*/
  updateRegisterRecord(json) {
    return http.Post("hospital/register/updateRecord", json, true);
  },
  /** 开始就诊*/
  doctorJieZhen(json) {
    return http.Post("config/api/cure/pre/reception", json, true);
  },

  //=======================================病历
  //修改病历
  bladdOrUpdate(json) {
    return http.Post("config/api/cure-dynamic-medical-additional/add-or-update", json, true);
  },
  //门诊分页查询新医生门诊模板数据
  newdoctorOutpatientTemplate(json) {
    return http.Get("config/api/doctorOutpatientTemplate/cure/pageQuery", json);
  },
  //历史病历
  patientHistorypageQueryV2(json) {
    return http.Get("config/api/cureMedical/patientHistory/pageQueryV2", json);
  },
  //根据ID查询模板数据
  getidtemplateDynamic(json) {
    return http.Get("config/api/templateDynamic/get/id", json);
  },
  //分页查询动态模板列表
  templateDynamicpageQueryDoctorUse(json) {
    return http.Get("config/api/templateDynamic/pageQueryDoctorUse", json);
  },
  //门诊默认动态模板查询
  defaulttemplateDynamic(json) {
    return http.Get("config/api/templateDynamic/cure/default-template", json);
  },

  //添加动态模板数据
  addtemplateDynamic(json) {
    return http.Post("config/api/templateDynamic/add", json, true);
  },
  //更新动态诊疗数据
  updatecureDynamicmedical(json) {
    return http.Post("config/api/cureDynamic/medical/update", json, true);
  },
  //添加动态病历数据
  addcureDynamicmedical(json) {
    return http.Post("config/api/cureDynamic/medical/add", json, true);
  },
  //门诊查询动态模板
  gettemplateDynamicdoctorIdOrDeptId(json) {
    return http.Get("config/api/templateDynamic/get/doctorIdOrDeptId", json);
  },
  //病历详情
  getcureDynamicdetailid(json) {
    // return http.Get("config/api/cureDynamic/detail/id", json);
    return http.Get("config/api/cureDynamic/detail-priority-additional/id", json);
  },
  //===========改版
  /** 病历详情 */
  getCureMedicalDetailId(json) {
    return http.Get("config/api/cureMedical/detail/id", json);
  },
  /** 添加病历 */
  addCureMedicalDetail(json) {
    return http.Post("config/api/cureMedical/addMultiple", json, true);
  },
  /** 修改病历 */
  updateCureMedicalDetail(json) {
    return http.Post("config/api/cureMedical/update", json, true);
  },
  /** 添加病历模板 */
  addCaseTemplate(json) {
    return http.Post("config/api/caseTemplate/addCaseTemplate", json, true);
  },
  /** 查询病历模板 */
  getCaseTemplatePageQuery(json) {
    return http.Get("config/api/caseTemplate/pageQuery", json);
  },
  /** 添加病历模板 */
  addCaseTemplate(json) {
    return http.Post("config/api/caseTemplate/addCaseTemplate", json, true);
  },
  /** 获取历史病历 */
  getPatientHistoryPageQuery(json) {
    return http.Get("config/api/cureMedical/patientHistory/pageQuery", json);
  },

  //=====================================初步诊断
  /** 新增 */
  addCureFirstVisitRecord(json) {
    return http.Post("config/api/cureFirstVisitRecord/add", json);
  },
  /** 查询 */
  getCureFirstVisitRecord(json) {
    return http.Get("config/api/cureFirstVisitRecord/pageQuery", json);
  },

  //===========================================处方
  /** 中药打印 */
  printtraditional(json) {
    return http.Get("config/api/curePrescription/traditional/print", json);
  },
  /** 西药打印 */
  printwesterncurePrescription(json) {
    return http.Get("config/api/curePrescription/western/print", json);
  },
  /** 添加中药 */
  addMultipletraditionalMedicine(json) {
    return http.Post("config/api/curePrescription/traditionalMedicine/addMultiple", json, true);
  },
  /** 修改中药 */
  updatetraditionalMedicine(json) {
    return http.Post("config/api/curePrescription/traditionalMedicine/update", json, true);
  },
  /** 添加西药 */
  addMultiplewesternMedicine(json) {
    return http.Post("config/api/curePrescription/westernMedicine/addMultiple", json, true);
  },
  /** 修改西药 */
  updatewesternMedicine(json) {
    return http.Post("config/api/curePrescription/westernMedicine/update", json, true);
  },
  /** 是否可以删除 */
  prescriptionDrugpreDelete(json) {
    return http.Get("config/api/curePrescription/prescriptionDrug/preDelete", json);
  },
  /** 查询患者历史处方 */
  prescriptionHistorylist(json) {
    return http.Get("config/api/curePrescription/prescriptionHistory/list", json);
  },
  /** 添加处方模板 */
  addWesternMedicineDrugTemplate(json) {
    return http.Post("config/api/westernMedicineDrugTemplate/add", json, true);
  },
  /** 分页处方模板 */
  getWesternMedicineDrugTemplatePageQuery(json) {
    return http.Get("config/api/westernMedicineDrugTemplate/pageQuery", json);
  },
  /** 查询处方模板详情 */
  getWesternMedicineDrugTemplateDetail(json) {
    return http.Get("config/api/westernMedicineDrugTemplate/detail/id", json);
  },
  /** 添加中药处方模板 */
  addTraditionalMedicineDrugTemplate(json) {
    return http.Post("config/api/traditionalMedicineDrugTemplate/addTraditionalMedicineDrugTemplate", json, true);
  },
  /** 查询中药处方模板 */
  getTraditionalMedicineDrugTemplatePageQuery(json) {
    return http.Get("config/api/traditionalMedicineDrugTemplate/pageQuery", json);
  },
  /** 查询中药处方模板详细 */
  getTraditionalMedicineDrugTemplateId(json) {
    return http.Get("config/api/traditionalMedicineDrugTemplate/detail/id", json);
  },

  /** 查询处方数据 */
  curePrescriptionlistcureId(json) {
    return http.Get("config/api/curePrescription/list/cureId", json);
  },
  /** 门诊分页查询西药处方模板列表 */
  westernMedicineDrugTemplateMZ(json) {
    return http.Get("config/api/westernMedicineDrugTemplate/cure/pageQuery", json);
  },
  /** 门诊分页查询中药处方模板列表 */
  traditionalMedicineDrugTemplatepageQueryMZ(json) {
    return http.Get("config/api/traditionalMedicineDrugTemplate/cure/pageQuery", json);
  },

  /** 处方收入统计 */
  getPrescriptionDrugPrescriptionIncome(json) {
    return http.Get("config/api/curePrescription/prescriptionDrug/prescriptionIncome", json);
  },

  //============================================药品
  /** 查询药品分类（树） */
  getMedicalDrugTypeTree(json) {
    return http.Get("config/api/medicalDrugType/getMedicalDrugTypeTree", json);
  },
  /** 查询中药分类（树） */
  getTraditionalMedicineTemplateSortTree(json) {
    return http.Get("config/api/traditionalMedicineTemplateSort/tree", json);
  },
  /** 分页查询患者药品发放信息 */
  curePrescriptionpageQuery(json) {
    return http.Get("config/api/curePrescription/pageQuery", json);
  },
  /** 患者药品发放统计信息 */
  curePrescriptionstatistictype(json) {
    return http.Get("config/api/curePrescription/statistic/type", json);
  },
  /** 药房-发药列表 */
  curePrescriptiondispensingDrug(json) {
    return http.Get("config/api/curePrescription/dispensingDrug", json);
  },
  /** 通过药品id的集合找到药品的批次 */
  getStockBatchByDrug(json) {
    return http.Get("config/api/medicalStock/getStockBatchByDrug", json);
  },
  /** 发药 */
  prescriptionSendDrug(json) {
    return http.Post("config/api/medicalDrugRetail/prescriptionSendDrugV2", json, true);
  },

  /**
   * 查询可退药列表
   * @param json
   * @returns {Promise<any>}
   */
  getRefundableMedicineList(json) {
    return http.Get("config/api/medicalDrugRetail/queryRefundableDrugs", json);
  },

  /**
   * 处方退药
   * @param json
   * @returns {Promise<any>}
   */
  returnMedicine(json) {
    return http.Post("config/api/medicalDrugRetail/prescriptionWithdrawal", json, true);
  },

  /**
   * 查询处方已退药列表
   * @param json
   * @returns {Promise<any>}
   */
  getReturnedMedicineList(json) {
    return http.Get("config/api/medicalDrugRetail/queryDrugWithdrawalList", json);
  },

  /**
   * 查询已退费列表
   * @param json
   * @returns {Promise<any>}
   */
  getHaveRefundList(json) {
    return http.Get("config/api/medicalDrugRetail/queryRefundList", json);
  },

  /**
   *查询处方已退费列表总金额
   * @param json
   * @returns {Promise<any>}
   */
  getRefundTotalAmount(json) {
    return http.Get("config/api/medicalDrugRetail/queryRefundListTotalAmount", json);
  },

  //========================治疗
  /** 开始治疗 */
  cureAdviceconfirmCure(json) {
    return http.Post("config/api/cureAdvice/confirmCure", json, true);
  },

  /** 治疗项目列表 */
  cureAdvicelistCureProgram(json) {
    return http.Get("config/api/cureAdvice/listCureProgram", json);
  },
  /** 是否可以删除 */
  cureAdvicepreDelete(json) {
    return http.Get("config/api/cureAdvice/cureAdvice/preDelete", json);
  },
  /** 获取资料医嘱 */
  getMedicalAdvicePageList(json) {
    return http.Get("config/api/medicalAdvice/getMedicalAdvicePageList", json);
  },
  /** 添加患者治疗表数据 */
  addCureAdvice(json) {
    return http.Post("config/api/cureAdvice/addMultiple", json, true);
  },
  /** 获取治疗模板 */
  getTreatmentTemplatePageQuery(json) {
    return http.Get("config/api/treatmentTemplate/pageQuery", json);
  },
  /** 获取治疗模板详细 */
  getTreatmentTemplateDetail(json) {
    return http.Get("config/api/treatmentTemplate/detail/id", json);
  },
  /** 更新患者治疗 */
  updatecureAdvice(json) {
    return http.Post("config/api/cureAdvice/update", json, true);
  },
  /** 根据诊疗ID查询患者治疗信息 */
  cureAdvicelistcureId(json) {
    return http.Get("config/api/cureAdvice/list/cureId", json);
  },
  /** 查询患者治疗项目 */
  cureAdvicepageQuery(json) {
    return http.Get("config/api/cureAdvice/pageQuery", json);
  },
  /** 统计信息 */
  cureAdvicestatistictype(json) {
    return http.Get("config/api/cureAdvice/statistic/type", json);
  },
  /** 查询待治疗项目 */
  cureAdvicepreConfirmedCheckcureId(json) {
    return http.Get("config/api/cureAdvice/preConfirmedCheck/cureId", json);
  },
  /** 治疗确认 */
  confirmedCheckcureAssistIds(json) {
    return http.Post("config/api/cureAdvice/confirmedCheck/cureAssistIds", json, true);
  },

  //======================================实验室
  /** 获取医学检查详情 */
  medicalcheckupv2detailid(json) {
    return http.Get("config/api/medical-checkup/v2/detail/id", json);
  },
  /** 获取医学检查详情 */
  medicalcheckupdetailid(json) {
    return http.Get("config/api/medical-checkup/detail/id", json);
  },
  /** 检查确认前置列表查询V2 */
  cureCheckupV2preConfirmedCheckcureId(json) {
    return http.Get("config/api/cureCheckup/V2/preConfirmedCheck/cureId", json);
  },
  /** 分页查询患者实验室检查信息 */
  cureCheckuppageQueryV2(json) {
    return http.Get("config/api/cureCheckup/pageQueryV2", json);
  },

  /** 医学检查分类树获取 */
  medicalcheckuptypetree(json) {
    return http.Get("config/api/medical-checkup-type/tree", json);
  },
  /** 分页查询医学检查列表 */
  medicalcheckuppageQuery(json) {
    return http.Get("config/api/medical-checkup/pageQuery", json);
  },
  /** 分页查询医学检查列表 */
  cureCheckupaddMultipleV2(json) {
    return http.Post("config/api/cureCheckup/addMultipleV2", json, true);
  },
  /** 更新实验室检查V2 */
  cureCheckupupdateV2(json) {
    return http.Post("config/api/cureCheckup/updateV2", json, true);
  },

  /** 实验室检查列表查询 */
  listcheckupcureId(json) {
    return http.Get("config/api/cureCheckup/list-checkup/cureId", json);
  },
  /** 是否可以删除 */
  cureCheckuppreDelete(json) {
    return http.Get("config/api/cureCheckup/cureCheckup/preDelete", json);
  },
  /** 分页查询实验室检查模板列表 */
  getLaboratoryInspectionTemplatePageList(json) {
    return http.Get("config/api/laboratoryInspectionTemplate/pageQueryV2", json);
  },
  /** 根据条件查询实验室检查列表 */
  getCheckupPageList(json) {
    return http.Get("config/api/checkup/getCheckupPageList", json);
  },
  /** 添加患者检验检查表数据 */
  addMultipleCureCheckup(json) {
    return http.Post("config/api/cureCheckup/addMultiple", json, true);
  },
  /** 找到一个检查的详情 */
  getCheckUpOne(json) {
    return http.Get("config/api/checkup/getCheckUpOne", json);
  },
  /** 查询患者实验室检查 */
  cureCheckuplistcureId(json) {
    return http.Get("config/api/cureCheckup/list/cureId", json);
  },
  /** 修改实验室检查 */
  updatecureCheckup(json) {
    return http.Post("config/api/cureCheckup/update", json, true);
  },
  /** 查询实验室患者 */
  cureCheckuppageQuery(json) {
    return http.Get("config/api/cureCheckup/pageQuery", json);
  },
  /** 查询状态统计 */
  cureCheckupstatistictype(json) {
    return http.Get("config/api/cureCheckup/statistic/type", json);
  },
  /** 查询患者待检查项目 */
  preConfirmedCheckcureId(json) {
    return http.Get("config/api/cureCheckup/preConfirmedCheck/cureId", json);
  },
  /** 确认检查 */
  confirmedCheckcureCheckupIds(json) {
    return http.Post("config/api/cureCheckup/confirmedCheck/cureCheckupIds", json, true);
  },
  /** 通过就诊id查询检查报告 */
  findCheckListByCure(json) {
    return http.Post("config/pui/lis/findCheckListByCureV2", json);
  },
  /** 通过检查id找到检查详情 */
  lisfindCheckDetail(json) {
    return http.Post("config/pui/lis/findCheckDetail", json);
  },

  //=================================辅助检查
  /**填写检查结果 */
  cureAssistcheckResult(json) {
    return http.Post("config/api/cureAssist/checkResult", json, true);
  },
  /** 辅助检查列表-检查中 */
  cureAssistchecking(json) {
    return http.Get("config/api/cureAssist/checking", json);
  },
  /** 是否可以删除 */
  cureAssistpreDelete(json) {
    return http.Get("config/api/cureAssist/cureAssist/preDelete", json);
  },
  /** 根据条件查询辅助检查项目列表 */
  getCheckAssistPageList(json) {
    return http.Get("config/api/checkupAssist/getCheckAssistPageList", json);
  },
  /** 添加治疗辅助检查表数据 */
  addMultipleCureAssist(json) {
    return http.Post("config/api/cureAssist/addMultiple", json, true);
  },
  /** 根据诊疗ID查询辅助检查信息 */
  getCureAssistCureId(json) {
    return http.Get("config/api/cureAssist/list/cureId", json);
  },
  /** 查找辅助检查结果详情 */
  cureAssistdetail(json) {
    return http.Get("config/api/cureAssist/detail", json);
  },
  /** 分页查询辅助检查模板列表 */
  assistInspectionTemplatePageQuery(json) {
    return http.Get("config/api/assistInspectionTemplate/pageQuery", json);
  },
  /** 更新治疗辅助检查表数据 */
  updateCureAssist(json) {
    return http.Post("config/api/cureAssist/updateCureAssist", json, true);
  },
  /** 分页查询患者辅助检查信息 */
  cureAssistpageQuery(json) {
    return http.Get("config/api/cureAssist/pageQuery", json);
  },
  /** 查询状态统计 */
  cureAssiststatistictype(json) {
    return http.Get("config/api/cureAssist/statistic/type", json);
  },
  /** 查询患者待检查项目 */
  cureAssistpreConfirmedCheckcureId(json) {
    return http.Get("config/api/cureAssist/preConfirmedCheck/cureId", json);
  },
  /** 确认检查 */
  cureAssistconfirmedCheckcureAssistIds(json) {
    return http.Post("config/api/cureAssist/confirmedCheck/cureAssistIds", json, true);
  },
  //====================================材料
  /** 是否可以删除 */
  cureMaterialpreDelete(json) {
    return http.Get("config/api/cureMaterial/cureMaterial/preDelete", json);
  },
  /** 分页查询物资收费信息 */
  pageQueryMaterialFee(json) {
    return http.Get("config/api/materialFee/pageQueryMaterialFee", json);
  },
  /** 添加治疗材料表数据 */
  addMultipleCureMaterial(json) {
    return http.Post("config/api/cureMaterial/addMultiple", json, true);
  },
  /** 根据诊疗ID查询材料费信息 */
  getCureMaterialCureId(json) {
    return http.Get("config/api/cureMaterial/list/cureId", json);
  },
  /** 分页查询物资模板列表（含库存） */
  materialTemplatePageQuery(json) {
    return http.Get("config/api/materialTemplate/pageQueryWithInventory", json);
  },
  /** 更新治疗材料表数据 */
  updateCureMaterial(json) {
    return http.Post("config/api/cureMaterial/update", json, true);
  },
  //查询材料（含库存）
  pageQueryWithInventory(json) {
    return http.Get("config/api/materialFee/pageQueryWithInventory", json);
  },

  //==============================其他收费
  /** 是否可以删除 */
  cureOtherspreDelete(json) {
    return http.Get("config/api/cureOthers/cureOthers/preDelete", json);
  },
  /** 添加患者其他收费 */
  addMultiplecureOthers(json) {
    return http.Post("config/api/cureOthers/addMultiple", json, true);
  },

  /** 分页查询其他费用列表 */
  pageOthersFee(json) {
    return http.Get("config/api/othersFee/pageOthersFee", json);
  },
  /** 查询其他费用模板 */
  othersFeeTemplatepageOthersFee(json) {
    return http.Get("config/api/othersFeeTemplate/pageOthersFee", json);
  },
  /** 添加患者其他收费表数据 */
  updatecureOthers(json) {
    return http.Post("config/api/cureOthers/update", json, true);
  },
  /** 根据诊疗ID查询其他费用数据 */
  cureOtherslistcureId(json) {
    return http.Get("config/api/cureOthers/list/cureId", json);
  },
  //===================================诊疗
  /** 查询是否有就诊费用 */
  preCureRegisterOrder(json) {
    return http.Get("config/api/cure/preCureRegisterOrder", json);
  },
  /** 查询诊疗费项目 */
  medicalExpensepageQuery(json) {
    return http.Get("hospital/doctor/getDoctorRegisterType", json);
  },
  //=====================混合模板
  /** 医生门诊模板 */
  getdoctorOutpatientTemplate(json) {
    return http.Get("config/api/doctorOutpatientTemplate/pageQuery", json);
  },
  /** 医生门诊模板详情 */
  doctorOutpatientTemplateByid(json) {
    return http.Get("config/api/doctorOutpatientTemplate/get/id", json);
  },
  /** 医生门诊模板详情2 */
  doctorOutpatientTemplatecureid(json) {
    return http.Get("config/api/doctorOutpatientTemplate/cure/get/id", json);
  },
  //============================套餐
  /** 获取全部套餐项目数据 */
  findPackageProjectAll(json) {
    return http.Get("user/packageProject/findPackageProjectAll", json);
  },
  /** 根据ID查找套餐二级项目数据 */
  findPackageProjectItemById(json) {
    return http.Get("user/packageProjectItem/findPackageProjectItemById", json);
  },
  /** 获取全部套餐二级项目数据 */
  findPackageProjectItemAll(json) {
    return http.Get("user/packageProjectItem/findPackageProjectItemAll", json);
  },
  /** 获取全部用户拥有套餐项目数据 */
  findUserPackageProjectAll(json) {
    return http.Get("user/userPackageProject/findUserPackageProjectAll", json);
  },
  /** 获取全部用户拥有套餐二级项目数据 */
  findUserPackageProjectItemAll(json) {
    return http.Get("user/userPackageProject/findUserPackageProjectItemAll", json);
  },
  /** 获取全部用户拥有套餐子项数据 */
  findUserPackageProjectItemDetailAll(json) {
    return http.Get("user/userPackageProject/findUserPackageProjectItemDetailAll", json);
  },
  /** 获取套餐树 */
  getItemListAllById(json) {
    return http.Get("user/packageProject/getItemListAllById", json);
  },
  /** 套餐详情查询 */
  medicalcombodetail(json) {
    return http.Get("config/api/medical-combo/detail/id", json);
  },
  /** 获取全部套餐项目并标注该患者是否拥有 */
  getAllProjectAndReturnUserHave(json) {
    return http.Get("user/userPackageProject/getAllProjectAndReturnUserHave", json);
  },
  /** 获取全部套餐二级项目并标注该患者是否拥有 */
  getAllProjectItemAndReturnUserHave(json) {
    return http.Get("user/userPackageProject/getAllProjectItemAndReturnUserHave", json);
  },
  /** 获取全部用户套餐核销记录表数据 */
  findUserPackageUseRecordAll(json) {
    return http.Get("user/userPackageUseRecord/findUserPackageUseRecordAll", json);
  },
  /** 根据ID查找用户套餐核销记录表数据 */
  findUserPackageUseRecordById(json) {
    return http.Get("user/userPackageUseRecord/findUserPackageUseRecordById", json);
  },
  /** 获取全部用户套餐核销子项记录数据 */
  findUserPackageUseRecordItemAll(json) {
    return http.Get("user/userPackageUseRecordItem/findUserPackageUseRecordItemAll", json);
  },
  /** 获取全部套餐具体子项目数据 */
  findPackageProjectItemDetailAll(json) {
    return http.Get("user/packageProjectItemDetail/findPackageProjectItemDetailAll", json);
  },
  /** 获取用户未拥有的套餐数据 */
  dontHaveUserPackageProjectAll(json) {
    return http.Get("user/userPackageProject/dontHaveUserPackageProjectAll", json);
  },
  /** 获取用户未拥有的套餐二级数据 */
  dontHaveUserPackageProjectItemAll(json) {
    return http.Get("user/userPackageProject/dontHaveUserPackageProjectItemAll", json);
  },
  /** 用来判断是否支付套餐 */
  getUserPackageItemById(json) {
    return http.Get("user/userPackageProject/getUserPackageItemById", json);
  },
  //--------------------------量表
  /** 获取最近一次答题记录详情（具体答案列表等）-通过量表id和患者id */
  getScaleAnswerRecordDetailScaleIdAndPatientId(json) {
    return http.Get("hospital/scaleAnswerRecord/getScaleAnswerRecordDetailScaleIdAndPatientId", json);
  },
  /** 根据患者Id查患者回答过的量表 */
  findScaleDetailByPatientId(json) {
    return http.Get("hospital/scale/findScaleDetailByPatientId", json);
  },
  // 通过类型获取来源列表
  getRecommendDetailListByType(json) {
    return http.Get("user/organization/getRecommendDetailListByType", json);
  }
};
export { yApi };
