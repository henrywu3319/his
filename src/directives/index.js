/**
 * Created with JavaScript.
 * User: rgxmg
 * Email: rgxmg@foxmail.com
 * Date: 2021/7/19
 * Time: 15:06
 *
 */
import vueRef from 'vue-ref';
import Vue from 'vue';

Vue.use(vueRef);
