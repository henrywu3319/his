import Vue from "vue";
import Vuex from "./plugins/vuex.js";
import Router from "./plugins/router.js";
import "./plugins/element.js";
import "./_css/style.scss";
import "./_js/load-file.js";
import "./_config/mixin.js";
import "./_config/api.js";
import "./_js/filter.js";
import "./components/UI/install";
import "./directives";
Vue.component("imagePath", () => import("./components/imagePath.vue"));
Vue.component("pager", () => import("./components/pager.vue"));
Vue.config.productionTip = false;
window.$vue = new Vue({
  router: Router,
  store: Vuex,
  data: {
    vw: 0,
    vh: 0,
    showView: true, //是否显示view
    systemCode: "002"
  },
  created() {
    let th = this;
    th.init();
  },
  mounted() {
    let th = this;
    //判断参数是否被改动
    let token = window.sessionStorage.getItem("token");
    let adminUser = window.sessionStorage.getItem("adminUser");
    if (adminUser) {
      th.$utils.isObject("hex_md5", function() {
        let strUser = window.hex_md5(adminUser + th.$config.publicKey);
        if (token != strUser) {
          th.$router.replace("/?nologin=true");
        }
      });
    }
    th.getArea();
  },
  methods: {
    init() {
      let th = this;
      th.vw = document.body.clientWidth;
      th.vh = document.body.clientHeight;
      th.JudgeMini();
      window.onresize = function() {
        th.vw = document.body.clientWidth;
        th.vh = document.body.clientHeight;
        th.JudgeMini();
      };
    },
    //自动收起菜单
    JudgeMini() {
      let th = this;
      if (th.vw < 1200) {
        th.$store.commit("Set_menuMini", true);
      } else {
        th.$store.commit("Set_menuMini", false);
      }
    },
    //获取地区
    getArea() {
      let th = this;
      th.$api.findCoreRegionAll("", "", 3).then(res => {
        if (res.info == "OK") {
          let arr = res.districts[0].districts;
          fn(arr);
          th.$store.state.areaArray = arr;
        }
      });
      const fn = function(list) {
        list.forEach(ele => {
          if (ele.districts && ele.districts.length > 0) fn(ele.districts);
          else delete ele.districts;
        });
      };
    }
  },
  watch: {}
}).$mount("#app");
