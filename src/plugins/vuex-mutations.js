// import Vue from "vue";
const mutations = {
  //----------------------------以下项目基础属性
  Set_curPage(state, value) {
    state.curPage = value;
  },
  Set_qxId(state, id) {
    state.qxId = id;
  },
  Set_menuArray(state, arr) {
    state.menuArray = arr;
  }, 
  //全局加载
  Set_vloading(state, value) {
    state.vloading = value;
  },
  //设置快捷导航下标
  Set_navRouterIndex(state, value) {
    state.navRouterIndex = value;
  },
  //设置快捷菜单
  set_navMenuList(state, arr) {
    state.navMenuList = arr;
  },
  //添加快捷导航
  Add_navMenuList(state, value) {
    //判断重复
    let isrepetition = false;
    for (let i = 0; i < state.navMenuList.length; i++) {
      let data = state.navMenuList[i];
      if (data.url == value.url) {
        isrepetition = true;
        if (value.name) data.name = value.name;
        if (value.sign) data.sign = value.sign;
        state.navRouterIndex = i;
        break;
      }
    }
    if (!isrepetition) {
      state.navMenuList.push(value);
      state.navRouterIndex = state.navMenuList.length - 1;
      //添加到缓存
      if (value.sign) state.includeCom.push(value.sign);
    }
  },
  //添加缓存
  add_includeCom(state, value) {
    let index = state.includeCom.findIndex(d => {
      return d == value;
    });
    if (index == -1) {
      state.includeCom.push(value);
    }
  },
  //删除缓存组件(组件名称)null全删除
  Del_includeCom(state, name) {
    if (name === "") return;
    if (name) {
      let index = -1;
      for (let i = 0; i < state.includeCom.length; i++) {
        let data = state.includeCom[i];
        if (data == name) {
          index = i;
          break;
        }
      }
      //删除
      if (index != -1) state.includeCom.splice(index, 1);
    } else { 
      if (state.includeCom.length > 1)
        state.includeCom.splice(1, state.includeCom.length - 1);
    }
  },
  /**设置登录用户 */
  Set_adminUser(state, user) {
    state.adminUser = user;
  },
  Set_menuMini(state, value) {
    state.menuMini = value;
  },
  //==================================医生门诊
  // Set_isDoctorEdit(state, value) {
  //   state.doctorMZ.isDoctorEdit = value;
  // },
  Set_templateData(state, value) {
    state.doctorMZ.templateData = value;
  },
  Set_templateSave(state, obj) {
    state.doctorMZ.templateSave[obj.key] = obj.value;
  },
  Clear_templateSave(state) {
    for (let key in state.doctorMZ.templateSave) {
      state.doctorMZ.templateSave[key] = false;
    }
  },
  //自动开启保存提示
  Set_templateSaveAuto(state, obj) {
    for (let key in state.doctorMZ.templateSave) {
      if (obj[key]) {
        state.doctorMZ.templateSave[key] = true;
      }
    }
  },
  Set_curTabIndex(state, value) {
    state.doctorMZ.curTabIndex = value;
  },
  Set_patientInfo(state, value) {
    state.doctorMZ.patientInfo = value;
  },
  Set_doctorMzEnum(state, obj) {
    Object.assign(state.doctorMZ.enum, obj);
  },
  Clear_MzEnum(state) {
    for (let key in state.doctorMZ.enum) {
      state.doctorMZ.enum[key] = [];
    }
  },
  //赋值门诊需要的ID
  Set_MzIds(state, obj) {
    Object.assign(state.doctorMZ, obj);
  },
  //清空门诊的id
  Clear_MzIds(state) {
    state.doctorMZ.patientId = "";
    state.doctorMZ.doctorId = "";
    state.doctorMZ.deptId = "";
    state.doctorMZ.cureId = "";
  }
};
export default mutations;
