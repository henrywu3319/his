import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);
//----重要提示
//1:vue文件的name必须和路由path相同(用来加缓存)
//2:meta.path：用来标注子级页面是跟那个父级页面管理的，值跟关联页面的path一样。如果该值存在子级页面的权限将采用关联页面的权限来进行验证
//3:本项目结构不支持3级菜单

//营销管理
const marketing = [
  {
    path: "paiban",
    component: () => import("@/pages/marketing/paiban.vue")
  },
  {
    path: "paibantype",
    meta: {
      path: "paiban"
    },
    component: () => import("@/pages/marketing/paibantype.vue")
  },
  {
    path: "reservecentre",
    component: () => import("@/pages/marketing/reservecentre.vue")
  },
  {
    path: "registerQueue",
    component: () => import("@/pages/marketing/registerQueue.vue")
  },
  {
    path: "bfregisterQueue",
    component: () => import("@/pages/marketing/bfregisterQueue.vue")
  },
  {
    path: "sourceNum",
    component: () => import("@/pages/marketing/sourceNum.vue")
  }
];
//患者管理
const patient = [
  {
    path: "visitDetail",
    meta: {
      path: "visit"
    },
    component: () => import("@/pages/patient/visitDetailV2.vue")
  },
  {
    path: "editVisit",
    meta: {
      path: "visit"
    },
    component: () => import("@/pages/patient/editVisitV2.vue")
  },
  {
    path: "visit",
    component: () => import("@/pages/patient/visitV2.vue")
  },
  {
    path: "cancelVisitDetail",
    component: () => import("@/pages/patient/cancelVisitDetail.vue")
  },
  {
    path: "completeVisitDetail",
    component: () => import("@/pages/patient/completeVisitDetail.vue")
  },
  {
    path: "userSign",
    meta: {
      path: "patientList"
    },
    component: () => import("@/pages/patient/_child/userSign.vue")
  },
  {
    path: "fastRegister",
    component: () => import("@/pages/fastRegister/index.vue")
  },
  {
    path: "patientList",
    component: () => import("@/pages/patient/list.vue")
  },
  {
    path: "addPatient",
    meta: {
      path: "patientList"
    },
    component: () => import("@/pages/patient/add.vue")
  },
  {
    path: "userDetail",
    name: "userDetail",
    // meta: {
    //   path: "patientList",
    // },
    component: () => import("@/pages/patient/detail.vue")
  },
  {
    path: "sugarDetail",
    meta: {
      path: "patientList"
    },
    component: () => import("@/pages/patient/_child/sugarDetail.vue")
  },
  {
    path: "childExam",
    meta: {
      path: "patientList"
    },
    component: () => import("@/pages/patient/_child/new_childExam.vue")
  },
  {
    path: "childreport",
    meta: {
      path: "patientList"
    },
    component: () => import("@/pages/patient/_child/child_report.vue")
  }
];
//就诊流程
const process = [
  {
    path: "register",
    component: () => import("@/pages/process/register.vue")
  },
  {
    path: "fenzhen",
    component: () => import("@/pages/process/fenzhen.vue")
  },
  {
    path: "doctorMenZhen",
    component: () => import("@/pages/process/doctorMenZhen.vue")
  },
  {
    path: "jieZhen",
    meta: {
      path: "doctorMenZhen"
    },
    component: () => import("@/pages/process/jieZhen.vue")
  },
  {
    path: "laboratory",
    component: () => import("@/pages/laboratory/laboratory.vue")
  },
  {
    path: "labPatientDetail",
    meta: {
      path: "laboratory"
    },
    component: () => import("@/pages/laboratory/labPatientDetail.vue")
  },
  {
    path: "assistcheck",
    component: () => import("@/pages/assistcheck/assistcheck.vue")
  },
  {
    path: "assistcheckDetail",
    meta: {
      path: "assistcheck"
    },
    component: () => import("@/pages/assistcheck/patientDetail.vue")
  },
  {
    path: "curecheck",
    component: () => import("@/pages/curecheck/curecheck.vue")
  },
  {
    path: "curepatient",
    meta: {
      path: "curecheck"
    },
    component: () => import("@/pages/curecheck/patientDetail.vue")
  },
  {
    path: "pharmacyindex",
    component: () => import("@/pages/drug/pharmacyindex.vue")
  },
  {
    path: "patientDrug",
    meta: {
      path: "pharmacyindex"
    },
    component: () => import("@/pages/drug/patientDrug.vue")
  }
];
//物资管理
const supplies = [
  {
    path: "suppliesIndex",
    component: () => import("@/pages/supplies/index.vue")
  },
  {
    path: "eidtsupplies",
    meta: {
      path: "suppliesIndex"
    },
    component: () => import("@/pages/supplies/_com/add.vue")
  },
  {
    path: "suppliesDetail",
    meta: {
      path: "suppliesIndex"
    },
    component: () => import("@/pages/supplies/_com/drugDetail.vue")
  },
  {
    //添加出库
    path: "editSuppliesStock",
    meta: {
      path: "suppliesIndex"
    },
    component: () => import("@/pages/supplies/_com/addstock.vue")
  },
  {
    //编辑库存
    path: "editSuppliesInventory",
    meta: {
      path: "suppliesIndex"
    },
    component: () => import("@/pages/supplies/_com/editInventory.vue")
  },
  {
    path: "suppliesInventoryDetail",
    meta: {
      path: "suppliesIndex"
    },
    component: () => import("@/pages/supplies/_com/inventoryDetail.vue")
  }
];

//药品库
const durg = [
  {
    path: "drugIndex",
    component: () => import("@/pages/drug/index.vue")
  },
  {
    path: "eidtDrug",
    meta: {
      path: "drugIndex"
    },
    component: () => import("@/pages/drug/_com/add.vue")
  },
  {
    path: "drugDetail",
    meta: {
      path: "drugIndex"
    },
    component: () => import("@/pages/drug/_com/drugDetail.vue")
  },
  {
    //出库
    path: "editstock",
    meta: {
      path: "drugIndex"
    },
    component: () => import("@/pages/drug/_com/addstock.vue")
  },
  {
    //编辑库存
    path: "editInventory",
    meta: {
      path: "drugIndex"
    },
    component: () => import("@/pages/drug/_com/editInventory.vue")
  },
  {
    path: "inventoryDetail",
    meta: {
      path: "drugIndex"
    },
    component: () => import("@/pages/drug/_com/inventoryDetail.vue")
  }
];
const receive = [
  {
    path: "receiveIndex",
    meta: {
      path: "receiveIndex"
    },
    component: () => import("@/pages/drugByDept/index.vue")
  },
  {
    path: "addReceive",
    meta: {
      path: "receiveIndex"
    },
    component: () => import("@/pages/drugByDept/_com/receive.vue")
  },
  {
    path: "receiveDetail",
    meta: {
      path: "drugIndex"
    },
    component: () => import("@/pages/drugByDept/_com/detail.vue")
  },
  {
    path: "cancelDetail",
    meta: {
      path: "receiveIndex"
    },
    component: () => import("@/pages/drugByDept/_com/cancelDetail.vue")
  }
];
const retail = [
  {
    path: "retailIndex",
    component: () => import("@/pages/retail/index.vue")
  },
  {
    path: "addRetail",
    meta: {
      path: "retailIndex"
    },
    component: () => import("@/pages/retail/add.vue")
  },
  {
    path: "retailDetail",
    meta: {
      path: "retailIndex"
    },
    component: () => import("@/pages/retail/detail.vue")
  }
];
const integral = [
  {
    path: "integralIndex",
    component: () => import("@/pages/integral/index.vue")
  }
];
//收费
const charge = [
  {
    path: "chargeIndex",
    component: () => import("@/pages/charge/index.vue")
  },
  {
    path: "chargeDetail",
    meta: {
      path: "chargeIndex"
    },
    component: () => import("@/pages/charge/_child/detail.vue")
  },
  {
    path: "paycharge",
    meta: {
      path: "chargeIndex"
    },
    component: () => import("@/pages/charge/pay_new.vue")
  },
  {
    path: "chargeReturn",
    meta: {
      path: "chargeIndex"
    },
    component: () => import("@/pages/charge/_child/return.vue")
  },
  {
    path: "chargeParticulars",
    meta: {
      path: "chargeIndex"
    },
    component: () => import("@/pages/charge/_child/particulars.vue")
  },
  {
    path: "chargeRepay",
    meta: {
      path: "chargeIndex"
    },
    component: () => import("@/pages/charge/_child/repay.vue")
  }
];
//收费V2
const chargeV2 = [
  {
    path: "chargeIndexV2",
    component: () => import("@/pages/chargeV2/index.vue")
  },
  {
    path: "chargeDetailV2",
    meta: {
      path: "chargeIndexV2"
    },
    component: () => import("@/pages/chargeV2/_child/tobepaidDetail.vue")
  },
  {
    path: "paychargeV2",
    meta: {
      path: "chargeIndexV2"
    },
    component: () => import("@/pages/chargeV2/payment.vue")
  },
  {
    path: "hasChargeDetail",
    meta: {
      path: "hasChargeDetail"
    },
    component: () => import("@/pages/chargeV2/_child/hasChargeDetail.vue")
  },
  {
    path: "hasRefundDetail",
    meta: {
      path: "hasRefundDetail"
    },
    component: () => import("@/pages/chargeV2/_child/hasRefundDetail.vue")
  },
  {
    path: "returnPremium",
    meta: {
      path: "returnPremium"
    },
    component: () => import("@/pages/chargeV2/_child/returnPremium.vue")
  },
  {
    path: "returnDetail",
    meta: {
      path: "returnDetail"
    },
    component: () => import("@/pages/chargeV2/_child/returnDetail.vue")
  }
  // {
  //   path: "chargeReturn",
  //   meta: {
  //     path: "chargeIndex",
  //   },
  //   component: () => import("@/pages/chargeV2/_child/return.vue"),
  // },
  // {
  //   path: "chargeParticulars",
  //   meta: {
  //     path: "chargeIndex",
  //   },
  //   component: () => import("@/pages/chargeV2/_child/particulars.vue"),
  // },
  // {
  //   path: "chargeRepay",
  //   meta: {
  //     path: "chargeIndex",
  //   },
  //   component: () => import("@/pages/chargeV2/_child/repay.vue"),
  // },
];
const chart = [
  {
    path: "chartIndex",
    component: () => import("@/pages/chart/index.vue")
  }
];
const help = [
  {
    path: "helpIndex",
    component: () => import("@/pages/help/index.vue")
  }
];
const other = [
  {
    path: "groupObject",
    component: () => import("@/pages/other/groupObject.vue")
  },
  {
    path: "groupObjDelRecord",
    component: () => import("@/pages/other/groupObjDelRecord.vue")
  }
];
const router = new Router({
  mode: "history",
  // base: process.env.BASE_URL,
  routes: [
    { path: "/", component: () => import("@/pages/login.vue") },
    {
      path: "/page",
      component: () => import("@/pages/app.vue"),
      children: [
        {
          path: "index",
          component: () => import("@/pages/index.vue")
        },
        ...marketing,
        ...patient,
        ...process,
        ...durg,
        ...supplies,
        ...integral,
        ...retail,
        ...charge,
        ...chart,
        ...help,
        ...other,
        ...receive,
        ...chargeV2
      ]
    }
  ]
});
router.beforeEach((to, from, next) => {
  //验证登录
  if (to.path != "/") {
    let adminUser = "";
    if (window.$vue) {
      adminUser = window.$vue.adminUser;
    } else {
      let storage = window.sessionStorage.getItem("adminUser");
      if (storage) {
        adminUser = JSON.parse(storage);
      }
    }
    if (adminUser && Object.keys(adminUser).length > 0) {
      next();
    } else {
      window.location.href = "/?nologin=true";
    }
  } else next();
});

export default router;
