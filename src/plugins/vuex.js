import Vuex from "vuex";
import Mutations from "./vuex-mutations.js";
Vue.use(Vuex);
const vuex = new Vuex.Store({
  state: {
    qxId: 0, //当前页面权限ID
    curPage: {}, //当前页面信息 
    adminUser: null, //登录的用户
    vloading: false, //小菊加载
    menuMini: false, //小菜单
    menuArray: [], //菜单
    navMenuList: [], //快捷导航
    includeCom: [], //需要缓存的组件
    navRouterIndex: 0, //快捷导航选中的下标
    areaArray:[],//全部城市
    //======================= 医生门诊 
    doctorMZ: {
      patientId:"",
      doctorId:"",
      deptId:"",
      cureId:"",
      // isDoctorEdit: false, //医生是否修改患者信息（医生门诊）
      patientInfo: {},//患者
      //选择的大模板数据{t0:{},t1:{}}
      templateData: {},
      curTabIndex: "t0",//当前选择的门诊标签
      //模板渲染是否已经保存
      templateSave: {
        t0: false,
        t1: false,
        t2: false,
        t3: false, 
        t4: false,
        t5: false,
        t6: false,
        t7: false,
        t8: false,
      },
      enum: {
        //职业
        occupationEnum: [],
        //民族
        nationEnum: [],
        //剂量单位
        doseUnitEnum: [],
        //用法
        useEnum: [],
        //频次
        frequencyEnum: [],
        //取药地点
        getAddressEnum: [],
        //单位
        unitEnum:[],
        //检查部位
        checkPartEnum:[],
      },
      
    }
  },
  mutations: Mutations,
  actions: {},
  getters: {
    get_adminUser(state) {
      if (state.adminUser) {
        return state.adminUser;
      } else {
        let admin = window.sessionStorage.getItem("adminUser");
        if (admin) {
          let j = JSON.parse(admin);
          state.adminUser = j;
          return j;
        } else {
          state.adminUser = {};
          return {};
        }
      }
    }
  }
});
export default vuex;
