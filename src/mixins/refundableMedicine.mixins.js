/**
 * Created with JavaScript.
 * User: rgxmg
 * Email: rgxmg@foxmail.com
 * Date: 2021/7/20
 * Time: 12:00
 * 可否退药验证
 * 根据传入的list集合进行处理
 * 要求格式: [{ curId: 1 }]
 */
export default {
  created() {
    this.getCommonConfigByKey("药品剂量单位").then(res => {
      this.refundableunitEnum = res;
    });
  },
  methods: {
    getRefundableEnums(value, list) {
      let str = "unknown";
      for (var i = 0; i < list.length; i++) {
        if (parseInt(value) == list[i].id) {
          if (list[i].desc != undefined) {
            str = list[i].desc;
            break;
          }
        }
      }
      return str;
    },

    /**
     * 更新某个item的可退药字段
     * @param item
     */
    updateRefundableToItem(item) {
      return this.extraRefundableToList([item]);
    },

    /**
     * 对list进行可退药的字段扩展
     * 增加
     * isReturnableMedicine 是否可退药
     * returnableMedicineList 可退药的list
     * @param list
     * @returns {Promise<unknown[]>}
     */
    extraRefundableToList(list) {
      console.log("list", list);
      return Promise.all(list.map(i => this.$api.getRefundableMedicineList({ id: i.cureId })))
        .then(data => {
          // 循环处理结果
          data.forEach((i, $i) => {
            // 取出可退药品的list
            const _list = (i || {}).dataBody || [];
            Object.assign(list[$i], {
              // 是否可退药
              isReturnableMedicine: !!_list.length,
              // 可退药的list
              returnableMedicineList: _list.map(i => ({ ...i, unitDesc: this.getRefundableEnums(i.unit, this.refundableunitEnum) }))
            });
          });
          return list;
        })
        .catch(e => {
          console.error(e.message);
          this.$message.error("系统错误，请稍后重试！");
          return list;
        });
    }
  }
};
