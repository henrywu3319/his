/**
 * Created with JavaScript.
 * User: rgxmg
 * Email: rgxmg@foxmail.com
 * Date: 2021/1/19
 * Time: 10:03
 *
 */
export default {
  size: String,

  width: [String, Number],

  height: [String, Number],

  maxHeight: [String, Number],

  fit: {
    type: Boolean,
    default: true,
  },

  stripe: Boolean,

  border: Boolean,

  rowKey: [String, Function],

  context: {},

  showHeader: {
    type: Boolean,
    default: true,
  },

  showSummary: Boolean,

  sumText: String,

  summaryMethod: Function,

  rowClassName: [String, Function],

  rowStyle: [Object, Function],

  cellClassName: [String, Function],

  cellStyle: [Object, Function],

  headerRowClassName: [String, Function],

  headerRowStyle: [Object, Function],

  headerCellClassName: [String, Function],

  headerCellStyle: [Object, Function],

  highlightCurrentRow: Boolean,

  currentRowKey: [String, Number],

  emptyText: String,

  expandRowKeys: Array,

  defaultExpandAll: Boolean,

  defaultSort: Object,

  tooltipEffect: String,

  spanMethod: Function,

  selectOnIndeterminate: {
    type: Boolean,
    default: true,
  },

  indent: {
    type: Number,
    default: 16,
  },

  treeProps: {
    type: Object,
    default() {
      return {
        hasChildren: 'hasChildren',
        children: 'children',
      };
    },
  },

  lazy: Boolean,

  load: Function,
};
