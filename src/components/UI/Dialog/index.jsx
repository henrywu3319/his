/**
 * Created with JavaScript.
 * User: RGXMG
 * Email: rickgrimes9229@gmail.com/759237309@qq.com
 * Date: 2020/8/26
 * Time: 21:25
 * 在elementUI的基础上增加了底部按钮
 * 以及在title及footer处增加border
 */
import { Dialog, Button } from "element-ui";
import { isPromise } from "@utils/dataHandler";
import styled from "./index.module.less";
export default {
  name: "SDialog",
  props: {
    visible: Boolean,
    // bodyStyle
    bodyStyle: {
      type: String,
      default: ""
    },
    footerStyle: {
      type: String,
      default: ""
    },
    title: {
      type: String,
      default: ""
    },

    modal: {
      type: Boolean,
      default: true
    },

    modalAppendToBody: {
      type: Boolean,
      default: true
    },

    appendToBody: {
      type: Boolean,
      default: true
    },

    lockScroll: {
      type: Boolean,
      default: true
    },

    closeOnClickModal: {
      type: Boolean,
      default: true
    },

    closeOnPressEscape: {
      type: Boolean,
      default: true
    },
    showOkButton: {
      type: Boolean,
      default: true
    },
    showCancelButton: {
      type: Boolean,
      default: true
    },

    showClose: {
      type: Boolean,
      default: true
    },

    width: String,

    fullscreen: Boolean,

    customClass: {
      type: String,
      default: ""
    },

    top: {
      type: String,
      default: "15vh"
    },
    beforeClose: Function,
    center: {
      type: Boolean,
      default: false
    },

    destroyOnClose: Boolean
  },
  components: {
    Button,
    Dialog
  },
  data() {
    return {
      // ok的loading
      loadingOfOk: false
    };
  },
  methods: {
    onClose() {
      if (this.visible) {
        // visible.async
        this.$emit("update:visible", false);
      }
      this.$emit("close");
    },
    onOk(e) {
      e.stopPropagation();
      const result = this.$listeners?.ok?.();
      if (isPromise(result)) {
        this.loadingOfOk = true;
        result.finally(() =>
          setTimeout(() => {
            this.loadingOfOk = false;
          }, 300)
        );
      }
    },
    onCancel(e) {
      e.stopPropagation();
      this.$emit("cancel");
      this.$emit("update:visible", false);
    }
  },
  render() {
    const props = {
      props: {
        ...this.$props
      },
      attrs: {
        ...this.$attrs
      },
      on: {
        close: this.onClose
      }
    };

    // 取出slots
    const { default: children, ...restChildren } = this.$slots;
    // 重置slots关系， 将上级传递的slots关系进行绑定，然后传递给子元素
    const slotsChildren = Object.keys(restChildren).reduce((m, k) => {
      let s = this.$slots[k][0];
      s.context = this._self;
      s.data = { slot: k };
      return [...m, s];
    }, []);
    return (
      <Dialog class="_sDialog-container" {...props}>
        {slotsChildren}
        <div style={this.bodyStyle} class={styled.bodyWrap}>
          {children}
        </div>
        {!this.$slots.footer && (
          <div slot="footer" style={this.footerStyle}>
            {this.showOkButton ? (
              <Button loading={this.loadingOfOk} onClick={this.onOk} type="primary">
                确 定
              </Button>
            ) : null}
            {this.showCancelButton ? <Button onClick={this.onCancel}>取 消</Button> : null}
          </div>
        )}
      </Dialog>
    );
  }
};
