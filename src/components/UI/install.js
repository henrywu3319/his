/**
 * Created with JavaScript.
 * User: rgxmg
 * Email: rgxmg@foxmail.com
 * Date: 2021/7/19
 * Time: 14:02
 *
 */
import Vue from "vue";
import Drawer from "./Drawer/index";
import Dialog from "./Dialog/index";
import Table from "./Table/index";

Vue.component("Drawer", Drawer);
Vue.component("Dialog", Dialog);
Vue.component("STable", Table);
