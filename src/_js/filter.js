//注意这里写全局过滤器，
import Config from "@/_config/config.js";
import Utils from "./utils.js";
let filter = [
  //图片
  {
    key: "imageUrl",
    callback: function(value, isBase = false) {
      let url = value;
      if (value && isBase == false) {
        let indexof = value.indexOf("http");
        if (indexof > -1) {
          url = value;
        } else {
          url = Config.imageUrl + value;
        }
      }
      return url;
    },
  },
  //时间格式化
  {
    key: "formatTime",
    callback: function(value, complete = true) {
      if (value === "" || value == null) return "";
      let result = "";
      if (complete) {
        result = Utils.FormatTime("yyyy-MM-dd hh:mm:ss", value);
      } else {
        result = Utils.FormatTime("yyyy-MM-dd", value);
      }
      return result;
    },
  },
  //转化金额为财务格式
  {
    key: "formatMoney",
    callback(value) {
      if (value == null) return "0";
      // value += "";
      let newstr = "";
      let newarr = value.toString().split(".");
      let g = 1;
      for (let i = newarr[0].length - 1; i >= 0; i--) {
        let a = newarr[0].substring(i + 1, i);
        if (g % 3 == 0) {
          if (newarr[0].length == g) {
            newstr = a + newstr;
          } else newstr = "," + a + newstr;
        } else {
          newstr = a + newstr;
        }
        g++;
      }
      if (newarr.length == 2) {
        newstr += "." + newarr[1];
      }
      return newstr;
    },
  },
  //将金额转成大写
  {
    key: "moneyToUpper",
    callback(value) {
      if (value == null) return "零";
      let dxarr = ["零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"];
      let val = "";
      let newvalue = value + "";
      for (let i = 0; i < newvalue.length; i++) {
        let n = newvalue.substring(i, i + 1);
        let index = Number.parseInt(n);
        if (!Number.isNaN(index)) val += dxarr[index];
        else val += n;
      }
      return val;
    },
  },
  //儿童展示实足年龄
  {
    key: "toTimeContrast",
    /** 计算特定两个时间的x年x月x日 */
    callback(birthday) {
      if (!birthday) return "";
      let now = new Date();
      let year = now.getFullYear();
      let month = now.getMonth() + 1;
      let day = now.getDate();
      let hour = now.getHours();
      let minute = now.getMinutes();
      let second = now.getSeconds();
      let myDate = new Date(birthday);
      let myYear = myDate.getFullYear();
      let myMonth = myDate.getMonth() + 1;
      let myDay = myDate.getDate();
      let myHour = myDate.getHours();
      let myMinute = myDate.getMinutes();
      let mySecond = myDate.getSeconds();
      const isLeapYear = function isLeapYear(year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
      };
      const getDaysOfMonth = function getDaysOfMonth(dateStr) {
        let date = new Date(dateStr);
        let year = date.getFullYear();
        let mouth = date.getMonth() + 1;
        let day = 0;

        if (mouth == 2) {
          day = isLeapYear(year) ? 29 : 28;
        } else if (
          mouth == 1 ||
          mouth == 3 ||
          mouth == 5 ||
          mouth == 7 ||
          mouth == 8 ||
          mouth == 10 ||
          mouth == 12
        ) {
          day = 31;
        } else {
          day = 30;
        }
        return day;
      };
      let gapSecond = second - mySecond;
      if (gapSecond < 0) {
        minute -= 1;
        gapSecond = 60 - mySecond + second;
      }
      let gapMinute = minute - myMinute;
      if (gapMinute < 0) {
        hour -= 1;
        gapMinute = 60 - myMinute + minute;
      }
      let gapHour = hour - myHour;
      if (gapHour < 0) {
        day -= 1;
        gapHour = 24 - myHour + hour;
      }
      let gapDay = day - myDay;
      if (gapDay < 0) {
        month -= 1;
        gapDay = getDaysOfMonth(birthday) - myDay + day;
      }
      let gapMonth = month - myMonth;
      if (gapMonth < 0) {
        year -= 1;
        gapMonth = 12 - myMonth + month;
      }
      let gapYear = year - myYear;
      if (gapYear < 0) {
        gapYear = 0;
      }
      let dateStr = `${gapYear}岁${gapMonth}月${gapDay}天`;
      return dateStr;
    },
  },
  //今天的取时分，往后的取年月日
  {
    key: "hourDate",
    callback(value) {
      let d = new Date(value);
      let cd = new Date();
      let str = "";
      let m, day;
      if (d.getDate() == cd.getDate()) {
        //今天的
        m = d.getHours();
        day = d.getMinutes();
        str =d.getFullYear() +
        "-" +
        (m < 10 ? "0" + m : m) +
        "-" +
        (day < 10 ? "0" + day : day)+"  " + (m < 10 ? "0" + m : m) + ":" + (day < 10 ? "0" + day : day);
      } else {
        m = d.getMonth() + 1;
        day = d.getDate();
        str =
          d.getFullYear() +
          "-" +
          (m < 10 ? "0" + m : m) +
          "-" +
          (day < 10 ? "0" + day : day);
      }
      return str;
    },
  },
  //计算两个两个时间相差多少小时和分钟
  {
    key: "computeHour",
    callback(value, time = new Date()) {
      let date = new Date(time);
      value = value.replace(/-/g, "/");
      let date2 = new Date(value);
      let time1 = date.getTime();
      let time2 = date2.getTime();
      let v = Math.abs(time1 - time2) / 1000;
      let str = "";
      let hour = Number.parseInt(v / 3600);
      if (hour > 0) str += hour + "小时";
      let minute = Math.round(Number.parseInt(v % 3600) / 60);
      str += minute + "分钟";
      return str;
    },
  },
  //返回时间的周几
  {
    key: "toWeek",
    callback(d) {
      switch (d.getDay()) {
        case 0:
          return "星期日";
        case 1:
          return "星期一";
        case 2:
          return "星期二";
        case 3:
          return "星期三";
        case 4:
          return "星期四";
        case 5:
          return "星期五";
        case 6:
          return "星期六";
      }
    },
  },
  //空数据
  {
    key: "emptyData",
    callback(value) {
      let str = "--";
      if (value === "" || value == null) return str;
      return value;
    },
  },
  //性别
  {
    key: "toSex",
    callback(value) {
      let str = "--";
      if (value == null) return str;
      else if (value == 0) return "不详";
      else return value == 1 ? "男" : "女";
    },
  },
  //获取时间
  {
    key: "changeTime",
    callback(value) {
      let str = "";
      if (value) {
        str = value.substring(0, 10);
      }
      return str;
    },
  },
  {
    key: "changeAssist",
    callback(value, list) {
      let str = "";
      // if(!value) return str;
      for (let i = 0; i < list.length; i++) {
        if (parseInt(value) == list[i].id) {
          if (list[i].desc != undefined) {
            str = list[i].desc;
            break;
          } else if (list[i].checkName != undefined) {
            str = list[i].checkName;
            break;
          }
        }
        if (parseInt(value) == list[i].labelId) {
          str = list[i].labelName;
          break;
        }
      }
      return str;
    },
  },
  {
    key: "changeSourceName",
    callback(value) {
      let str = "";
      switch (value) {
        case 1:
          str = "医保支付";
          break;
        case 2:
          str = "挂账金额";
          break;
        case 3:
          str = "商保支付";
          break;
        case 4:
          str = "使用卡券";
          break;
        case 5:
          str = "积分抵扣";
          break;
        case 6:
          str = "余额";
          break;
        case 7:
          str = "现金";
          break;
        case 8:
          "", (str = "支付宝");
          break;
        case 9:
          "", (str = "微信");
          break;
        case 10:
          "", (str = "银行卡");
          break;
        case 11:
          str = "找零";
          break;
        case 12:
          str = "赠送余额";
          break;
        case 13:
          str = "减免金额";
          break;
        // case 14:
        //   str = "套餐核销";
        //   break;
        case 14:
          str = "小程序余额";
          break;
        case 15:
          str = "POS机信用卡";
          break;
        case 16:
          str = "POS机储蓄卡";
          break;
        case 17:
          str = "打折金额";
          break;
        case 18:
          str = "POS机支付";
          break;
        case 19:
          str = "微信小助手";
          break;
      }
      return str;
    },
  },
  {
    key: "getBmi",
    callback(value) {
      let str = "";
      if (value) {
        value = Number(value);
        if (value <= 18.5) {
          str = "体型偏瘦";
        } else if (value > 18.5 && value < 23.9) {
          str = "体型正常";
        } else if (value > 23.9 && value < 27.9) {
          str = "体型过重";
        } else if (value > 27.9) {
          str = "体型肥胖";
        } else {
          str = "未知";
        }
      }
      return str;
    },
  },
  {
    key: "changeProject", //会员项目类型
    callback(value) {
      let str = "";
      switch (parseInt(value)) {
        case 1:
          str = "检验医嘱";
          break;
        case 2:
          str = "检查医嘱";
          break;
        case 3:
          str = "治疗医嘱";
          break;
        case 4:
          str = "处方医嘱";
          break;
        case 5:
          str = "诊疗费";
          break;
        case 6:
          str = "材料费";
          break;
        case 7:
          str = "其他收费";
          break;
      }
      return str;
    },
  },
  {
    key: "changePaySource", //支付方式
    callback(value) {
      let str = "";
      switch (parseInt(value)) {
        case 1:
          str = "医保";
          break;
        case 2:
          str = "挂账";
          break;
        case 3:
          str = "商保";
          break;
        case 4:
          str = "卡券";
          break;
        case 5:
          str = "积分";
          break;
        case 6:
          str = "余额";
          break;
        case 7:
          str = "现金";
          break;
        case 8:
          str = "支付宝";
          break;
        case 9:
          str = "微信";
          break;
        case 10:
          str = "银行卡";
          break;
        case 11:
          str = "找零";
          break;
        case 12:
          str = "赠送余额";
          break;
        case 14:
          str = "小程序余额";
          break;
        case 15:
          str = "POS机信用卡";
          break;
        case 16:
          str = "POS机储蓄卡";
          break;
      }
      return str;
    },
  },
  {
    key: "changeSource",
    callback(value) {
      let str;
      if (value !== "") {
        switch (value) {
          case 1:
            str = "使用积分";
            break;
          case 2:
            str = "增加积分";
            break;
          case 3:
            str = "清空积分";
            break;
        }
      }
      return str;
    },
  },
  {
    key: "changeMoneyToTwo",
    callback(value) {
      let str = "";
      if (value != "") {
        str = Number(value).toFixed(2);
      } else {
        str = 0.0;
      }
      return str;
    },
  },
  {
    key: "changePatientSource",
    callback(value) {
      let str = "";
      switch (value) {
        case 1:
          str = "亲友推荐";
          break;
        case 2:
          str = "微信";
          break;
        case 3:
          str = "媒体报道";
          break;
        case 4:
          str = "微博";
          break;
        case 5:
          str = "其他";
          break;
        case 6:
          str = "附近居民";
          break;
        case 7:
          str = "员工及家属";
          break;
        case 8:
          str = "活动推广";
          break;
        case 9:
          str = "病人传播";
          break;
        case 10:
          str = "介绍";
          break;
        default:
          str = "其他";
          break;
      }
      return str;
    },
  },
  {
    key: "changePaintName",
    callback(value) {
      let newStr;
      if (value.length === 2) {
        newStr = value.substr(0, 1) + "*" + value.substr(1, 1);
      } else if (value.length > 2) {
        let char = "";
        for (let i = 0, len = value.length - 2; i < len; i++) {
          char += "*";
        }
        newStr = value.substr(0, 1) + char + value.substr(-1, 1);
      } else {
        newStr = value;
      }
      return newStr;
    },
  },
  //西药规格
  {
    key: "durgSpec",
    callback(obj, unitEnum) {
      if (!obj) return "";
      const fn = function(val) {
        let d = unitEnum.find((d2) => {
          return d2.id == val;
        });
        if (d) {
          return d.desc;
        } else {
          return "";
        }
      };
      let str = `${obj.dose}${fn(obj.doseUnit)}*${obj.preparationNum}${fn(
        obj.preparationUnit
      )}`;
      return str;
    },
  },
  //药品库存
  {
    key: "durgInventory",
    callback(obj, unitEnum, type) {
      if (!obj) return type === "showZero" ? "0" : "";
      if (!obj.usableNum) type === "showZero" ? "0" : "";
      let str = "";
      const fn = function(val) {
        let d = unitEnum.find((d2) => {
          return d2.id == val;
        });
        if (d) {
          return d.desc;
        } else {
          return "";
        }
      };
      const toUn = function(obj) {
        let str = "";
        if (typeof obj != "undefined") {
          str = obj;
        }
        return str;
      };
      //可以拆分
      if (obj.isPiece == 1) {
        // str = `${Number.parseInt(obj.usableNum / obj.preparationNum)}${fn(
        //   obj.packUnit
        // )}${obj.usableNum % obj.preparationNum}${fn(obj.preparationUnit)}`;
        str = `${toUn(obj.packNum)}${fn(obj.packUnit)}${toUn(
          obj.subsectionNum
        )}${fn(obj.subsectionUnmUnit || "")}`;
      } else {
        str = `${toUn(obj.packNum)}${fn(obj.packUnit)}`;
      }
      return str;
    },
  },
  {
    key: "residueDurg",
    callback(obj, unitEnum) {
      let str = "";
      str = Utils.surplusDurg(obj, unitEnum);
      return str;
    },
  },
  //计算库存数量
  {
    key: "calculateStockNum",
    callback(obj, list) {
      if (obj) {
        //大单位数量
        let bigNum = parseInt(obj.totalQuantity / obj.preparationNum);
        let smallNum = Number(obj.totalQuantity % obj.preparationNum);
        let bigUnit = Utils.getDwByDwId(obj.packUnit, list);
        return obj.isPiece == 1
          ? bigNum +
              bigUnit +
              smallNum +
              Utils.getDwByDwId(obj.subsectionQuantityUnit, list)
          : bigNum + bigUnit;
        //  Utils.surplusDurg(obj.totalQuantityUnit, list);
      }
    },
  },
  //保留2位小数
  {
    key: "NumberToFixed",
    callback(value) { 
      return value!=''?value.toFixed(2):""
    }
  }
];
filter.forEach((item) => {
  Vue.filter(item.key, item.callback);
});
