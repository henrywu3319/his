const loadSource = {
  data: [
    {
      type: "css",
      url:
        process.env.VUE_APP_BF == "TRUE"
          ? "/font/iconfont/iconfont.css"
          : "//at.alicdn.com/t/font_1225678_i68r6feok6g.css"
    },
    {
      type: "script",
      async: true,
      url: "/js/md5.js"
    },
    // {
    //   type: "script",
    //   async: true,
    //   url: "/js/LodopFuncs.js"
    // },
    {
      type: "script",
      async: true,
      url: "/js/echarts.common.min.js"
    },
    {
      type: "script",
      async: true,
      url:
        process.env.VUE_APP_BF == "TRUE"
          ? "/js/swiper.min.js"
          : "https://cdn.bootcss.com/Swiper/4.4.6/js/swiper.min.js"
    },
    {
      type: "css",
      url:
        process.env.VUE_APP_BF == "TRUE"
          ? "/js/swiper.min.css"
          : "https://cdn.bootcss.com/Swiper/4.4.6/css/swiper.min.css"
    },
    {
      type: "css",
      url:
        process.env.VUE_APP_BF == "TRUE"
          ? "/js/wangEditor.min.css"
          : "https://cdn.bootcss.com/wangEditor/10.0.13/wangEditor.min.css"
    },
    {
      type: "script",
      async: true,
      url:
        process.env.VUE_APP_BF == "TRUE"
          ? "/js/wangEditor.min.js"
          : "https://cdn.bootcss.com/wangEditor/10.0.13/wangEditor.min.js"
    }
    // {
    //   type: "script",
    //   async: true,
    //   url:
    //     "https://webapi.amap.com/maps?v=1.4.14&key=fc898c8c41ac7519abd28864a35c1990"
    // },
  ],
  Load() {
    let th = this;
    let head = document.getElementsByTagName("head")[0];
    let body = document.getElementsByTagName("body")[0];
    th.data.forEach(item => {
      if (item.type == "css") {
        let link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = item.url;
        head.appendChild(link);
      } else {
        let script = document.createElement("script");
        script.type = "text/javascript";
        script.src = item.url;
        if (item.async) script.async = true;
        if (item.loadHead) head.appendChild(script);
        else body.appendChild(script);
      }
    });
  }
};
window.onload = function() {
  loadSource.Load();
};
