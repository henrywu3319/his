const Validate = {
  /**非中文 */
  NoHanZi(rule, value, callback) {
    if (escape(value).indexOf("%u") < 0) {
      callback();
    } else {
      callback(new Error("请输入非中文"));
    }
  },
  /**手机号 */
  vPhone(rule, value, callback) {
    let reg = /^\d{11}$/;
    if (value !== "" && typeof value != "undefined") {
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("请输入正确的手机号"));
      }
    } else {
      callback();
    }
  },
  /**验证身份证号 */
  vPeopleNumber(rule, idCard, callback) {
    let regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    if (idCard !== "" && typeof idCard != "undefined") {
      //如果通过该验证，说明身份证格式正确，但准确性还需计算
      if (regIdCard.test(idCard)) {
        if (idCard.length == 18) {
          let idCardWi = new Array(
            7,
            9,
            10,
            5,
            8,
            4,
            2,
            1,
            6,
            3,
            7,
            9,
            10,
            5,
            8,
            4,
            2
          ); //将前17位加权因子保存在数组里
          let idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
          let idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
          for (let i = 0; i < 17; i++) {
            idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
          }
          let idCardMod = idCardWiSum % 11; //计算出校验码所在数组的位置
          let idCardLast = idCard.substring(17); //得到最后一位身份证号码
          //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
          if (idCardMod == 2) {
            if (idCardLast == "X" || idCardLast == "x") {
              callback();
            } else {
              callback(new Error("请输入与正确的身份证号码"));
            }
          } else {
            //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
            if (idCardLast == idCardY[idCardMod]) {
              callback();
            } else {
              callback(new Error("请输入与正确的身份证号码"));
            }
          }
        }
      } else {
        callback(new Error("请输入与正确的身份证号码"));
      }
    } else {
      callback();
    }
  },
  /**整数和小数(不能负数) */
  integerAndFloat(rule, value, callback) {
    // let reg = /^[0-9]+([.]{1}[0-9]+){0,1}$/;
    let reg=/((^[1-9]\d*)|^0)(\.\d{0,2}){0,1}$/
    if (value !== "" && typeof value != "undefined") {
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("请输入整数或小数"));
      }
    } else {
      callback();
    }
  },
  /**正整数 */
  integerNum(rule, value, callback) {
    let reg = /^\d+$/;
    if (value !== "" && typeof value != "undefined") {
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("请输入整数"));
      }
    } else {
      callback();
    }
  },
  /**正整数**/
  integerNumber(rule, value, callback) {
    let reg = /^\d{1,6}$/;
    if (value !== "" && typeof value != "undefined") {
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("请输入正确6位以内的整数"));
      }
    } else {
      callback();
    }
  },
  //校验药品信息
  verifyDrugInfo(rule, obj, callback) {
    let reg = /^\d{1,6}$/;
    const fn =()=>{
      if(obj.applyUnit==obj.medicalDrugSale.packUnit){
        if(Number(obj.actualNum*obj.medicalDrugSale.preparationNum)>obj.inventoryStatisticVO.usableNum){
          callback(new Error("库存不足,请重新输入"))
        }else{
          callback();
        }
      }else{
        Number(obj.actualNum)>obj.inventoryStatisticVO.usableNum? callback(new Error("库存不足,请重新输入")):callback();
      }
    }
    if (obj !== "" && typeof obj != "undefined") {
      reg.test(obj.actualNum)?fn():callback(new Error("请输入正确6位以内的整数"));
      // reg.test(obj.actualNumSubsection)?fn():callback(new Error("请输入正确的库存数量"));
    } else {
      callback();
    }
  },
  /**保留1位小数 */
  FloatByTwo(rule, value, callback){
    let reg = /^(?=1\.[0-9]|[0-9]\.\d).{3}$|^([0-9])$/;
    if (value !== "" && typeof value != "undefined") {
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("请输入0~10之间的整数小数"));
      }
    } else {
      callback();
    }
  },
  //校验患者姓名字符串中是否有空格
  verigySpacing(rule, value, callback) { 
    if (typeof (value) == 'number') {
      callback();
    } else {
      let hreg = new RegExp("[\\u4E00-\\u9FFF]+", "g");
      if (hreg.test(value)) {
        let reg = new RegExp(/\s+/g);
        if (reg.test(value)) {
          console.log("1111");
          callback(new Error("输入有误，请剔除空格"));
        } else {
          console.log("333");
          callback();
        }
      } else { 
        console.log("222");
        callback();
      }
    }
  }
};
export default Validate;
