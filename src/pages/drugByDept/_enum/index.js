const enumArr={
    //  0待审核 1审核通过 2审核未通过 3已取消
    status:{
        0:'待审核',
        1:'审核通过',
        2:'审核未通过',
        3:'已取消'
    }
}
export default enumArr;