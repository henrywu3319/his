const xyMixin = {
  computed: {
    templateSave() {
      return this.$store.state.doctorMZ.templateSave;
    },
    patientInfo() {
      return this.$store.state.doctorMZ.patientInfo;
    },
    oneDoseUnitArr() {
      return this.$store.state.doctorMZ.enum.doseUnitEnum;
    },
    useArr() {
      return this.$store.state.doctorMZ.enum.useEnum;
    },
    frequencyArr() {
      return this.$store.state.doctorMZ.enum.frequencyEnum;
    },
    getAddressArr() {
      return this.$store.state.doctorMZ.enum.getAddressEnum;
    },
  },
  methods: {
    showUnit(id, obj) {
      let bo = false;
      if (obj.medicalDrugSale.isPiece == 1) {
        if (
          id == obj.medicalDrugSale.preparationUnit ||
          id == obj.medicalDrugSale.packUnit
        )
          bo = true;
      } else {
        if (id == obj.totalUnit) bo = true;
      }
      return bo;
    },
    judgeGroup() {
      let th = this;
      let isSuccess = true;
      let setArr = new Set();
      th.form.tableList.forEach((d) => {
        if (d.groupId) {
          setArr.add(d.groupId);
        }
      });
      for (let x of setArr) {
        let newArr = th.form.tableList.filter((data) => {
          return data.groupId == x;
        });
        if (newArr.length > 1) {
          let newData = newArr[0];
          let fArr = newArr.filter((dd) => {
            if (
              dd.frequency == newData.frequency &&
              dd.use == newData.use &&
              dd.useDay == newData.useDay
            )
              return true;
            else return false;
          });
          if (newArr.length != fArr.length) {
            isSuccess = false;
            break;
          }
        }
      }
      return isSuccess;
    },
    onOpenExplain(obj) {
      let th = this;
      th.curItem = obj;
      th.dialogExplain = true;
    },
    addFormList(json) {
      let th = this;
      if (typeof json.payStatus == "undefined") {
        json.payStatus = 2;
      }
      if (typeof json.status == "undefined") {
        json.status = 0;
      }
      if (json.payStatus == 2 && json.status == 0) {
        let data = th.form.tableList.find((d) => {
          return d.drugId == json.drugId && d.payStatus == 2 && d.status == 0;
        });
        if (data) {
          return;
        }
        json.groupId = json.groupId
          ? json.groupId
          : th.form.tableList.length + 1;
        th.calculateTotal();
        if (json.inventory && Object.keys(json.inventory).length > 0) {
          if (json.inventory.usableNum <= 0) {
            th.inventoryflaw(json);
          } else {
            th.form.tableList.push(json);
          }
        } else {
          th.inventoryflaw(json);
        }
      } else {
        th.form.tableList.push(json);
      }
    },
    inventoryflaw(json) {
      const th = this;
      th.$message.warning(json.drugName + "库存不足");
      if (th.form.tableList.length == 0) {
        th.$emit("submit");
      }
    },
    calculateTotal() {
      let th = this;
      let fn = (data) => {
        let pcObj = th.frequencyArr.find((d) => {
          return d.id == data.frequency;
        });
        if (!data.useDay) return;
        if (!pcObj.value) {
          data.useTotal = data.useDay * data.oneDose;
          return;
        }
        let totalCount = Math.ceil(
          (data.useDay * 24) / Number.parseInt(pcObj.value)
        );
        let total = data.oneDose * totalCount;
        if (data.inventory.isPiece == 1) {
          if (data.totalUnit == data.inventory.subsectionUnmUnit) {
            if (total > data.inventory.usableNum) {
              th.$message.warning("超过库存");
              data.useTotal = "";
            } else {
              data.useTotal = total;
            }
          } else {
            total = Math.ceil(
              total /
                (data.medicalDrugSale.preparationNum *
                  data.medicalDrugSale.dose)
            );
            if (total > data.inventory.packNum) {
              th.$message.warning("超过库存!");
              data.useTotal = "";
            } else {
              data.useTotal = total;
            }
          }
        } else {
          let preparationNum = data.medicalDrugSale.preparationNum || 1;
          data.useTotal = Math.ceil(
            total / (preparationNum * data.medicalDrugSale.dose)
          );
        }
      };
      for (const item of th.form.tableList) {
        if (item.payStatus !== 2 && item.status !== 0) {
          continue;
        }
        if (
          !item.oneDose ||
          !item.frequency ||
          !item.useDay ||
          !item.totalUnit
        ) {
          continue;
        }
        if (!item.medicalDrugSale) {
          item.medicalDrugSale = {
            preparationNum: 1,
            dose: 1,
          };
        }
        fn(item);
      }
    },
  },
};
export default xyMixin;
