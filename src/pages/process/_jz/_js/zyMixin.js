const zyMixin = {
  computed: {
    templateSave() {
      return this.$store.state.doctorMZ.templateSave;
    },
    patientInfo() {
      return this.$store.state.doctorMZ.patientInfo;
    },
    oneDoseUnitArr() {
      return this.$store.state.doctorMZ.enum.doseUnitEnum;
    },
    useArr() {
      return this.$store.state.doctorMZ.enum.useEnum;
    },
    frequencyArr() {
      return this.$store.state.doctorMZ.enum.frequencyEnum;
    },
    getAddressArr() {
      return this.$store.state.doctorMZ.enum.getAddressEnum;
    }
  },
  data() {
    return {
      rulesRequired: {
        required: true,
        message: "此项必填",
        trigger: ["blur", "change"]
      },
      rulesNumber: {
        message: "格式有误",
        type: "number",
        min: 1,
        trigger: ["blur", "change"]
      }
    };
  },
  methods: {
    
  }
};
export default zyMixin;
