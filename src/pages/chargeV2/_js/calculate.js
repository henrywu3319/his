
export default {
    /**
     * 详情折扣计算
     * @param {argument} type:1-折扣  2-原价
     * @param {type} 类型
     * @param {oldMoney} 原价
    */
    disCalculate(argument, type) { 
        if (type == 1) 
            return  (oldMoney) => { return Number(oldMoney * (argument / 10)).toFixed(2);}
        else 
            return  (oldMoney)=> {return Number(argument/oldMoney * 10).toFixed(1);} 
    },
    /**
     * 计算列表总金额
     */
    calculateTotalByList(list, bools) { 
        let oldTotalMoney = 0;
        if (bools) {
            oldTotalMoney = list.reduce((t, c) => {
                return t += Number(c.showAmount);
            }, 0)
        } else { 
            oldTotalMoney =  list.reduce((t,c) => { 
                return t += Number(c.totalAmount);   
            },0)
        }
        let newTotalMoney = list.reduce((t,c) => { 
            return t += Number(c.actualAmount);   
        }, 0)
        return { oldTotalMoney:oldTotalMoney,newTotalMoney:newTotalMoney,discountMoney:Number(oldTotalMoney-newTotalMoney) }
    }
}