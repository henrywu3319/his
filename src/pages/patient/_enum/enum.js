const enumArr = {
  rechargeArr: {
    6: "余额支付",
    7: "现金",
    8: "支付宝",
    9: "微信",
    10: "银行卡",
    18: "POS机支付",
    19: "微信小助手",
  },
  VISITSTATUS: {
    0: "待随访",
    1: "随访中",
    2: "已随访",
    3: "已取消",
  },
  VISITWAY: {
    1: "电话随访",
    2: "微信随访",
  },
  PAYSTATUS: {
    0: '未支付',
    1: '已支付',
    2:' 已退款'
  }
};
export default enumArr;
